"use strict";

require("dotenv").config();
const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");
const fileUpload = require("express-fileupload");
const app = express();

app.use(fileUpload());
app.use(express.json());
app.use(cors());

const productsRouter = require("./app/routes/products-routes");
const usersRouter = require("./app/routes/users-routes");
const reviewsRouter = require("./app/routes/reviews-routes");
const interactionsRouter = require("./app/routes/interaction-routes");
const transfersRouter = require("./app/routes/transfers-routes");
const politicsRouter = require("./app/routes/politics-routes");
const imagesProductsRouter = require("./app/routes/image-products-routes");
const chatsRouter = require("./app/routes/chats-routes");
const messagesRouter = require("./app/routes/messages-routes");

const port = process.env.SERVER_PORT || 3000;

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "./access.log"),
  { flags: "a" }
);
app.use(morgan("combined", { stream: accessLogStream }));

app.use("/api/v1/products/", productsRouter);
app.use("/api/v1/users/", usersRouter);
app.use("/api/v1/reviews/", reviewsRouter);
app.use("/api/v1/interactions/", interactionsRouter);
app.use("/api/v1/transfers/", transfersRouter);
app.use("/api/v1/politics/", politicsRouter);
app.use("/api/v1/imagesProducts", imagesProductsRouter);
app.use("/api/v1/chats", chatsRouter);
app.use("/api/v1/messages", messagesRouter);

app.listen(port, () => console.log(`Escuchando puerto ${port}`));
