"use strict";

const path = require("path");
const fs = require("fs");

async function isAdmin(rol) {
  if (rol !== "admin") {
    const error = new Error("No tienes permisos para realizar esta acción");
    error.status = 401;
    throw error;
  }
  return true;
}

async function deleteImageProfile(user) {
  const { PATH_USER_IMAGE } = process.env;
  const imagePath = PATH_USER_IMAGE;
  const pathProfileImageFolder = `${__dirname}/../../../public/${PATH_USER_IMAGE}`;
  if (user.image) {
    await fs.unlink(`${pathProfileImageFolder}/${user.image}`, () => {
      console.log("Borrada imagen de perfil correctamente");
    });
  }
}

module.exports = {
  isAdmin,
  deleteImageProfile,
};
