"use strict";

const nodemailer = require("nodemailer");

const {
  HTTP_SERVER_DOMAIN,
  SMTP_PORT,
  SMTP_HOST,
  SMTP_USER,
  SMTP_PASS,
  SMTP_FROM,
} = process.env;

const transporter = nodemailer.createTransport({
  port: SMTP_PORT,
  host: SMTP_HOST,
  auth: {
    user: SMTP_USER,
    pass: SMTP_PASS,
  },
  secure: false,
});

async function sendEmailRegistration(name, email, verificationCode) {
  const linkActivation = `${HTTP_SERVER_DOMAIN}/api/v1/users/activation?verification_code=${verificationCode}`;
  const mailData = {
    from: SMTP_FROM,
    to: email,
    subject:
      "Gracias por unirte a nuestra querida comunidad y sé bienvenido a AnTecQuary, donde la tecnología nunca pasará de moda",
    text: `Hola ${name}, Activa tu cuenta pinchando en el enlace: ${linkActivation}`,
    html: `Hola ${name}, Para confirmar tu cuenta <a href="${linkActivation}">activala aquí</a>`,
  };
  const data = await transporter.sendMail(mailData);

  return data;
}

async function sendEmailCorrectValidation(name, email) {
  const mailData = {
    from: SMTP_FROM,
    to: email,
    subject: "Tu cuenta en AnTecQuary se ha activado",
    text: `Hola ${name},\n Tu cuenta en AnTecQuary se ha activado. Disfruta de nuestros servicios retrotecnológicos`,
    html: `<p>Hola ${name},</p><p>Tu cuenta en AnTecQuary se ha activado. Disfruta de nuestros servicios retrotecnológicos</p>`,
  };

  const data = await transporter.sendMail(mailData);

  return data;
}

async function sendEmail(email, subject, text, html) {
  const mailData = {
    from: SMTP_FROM,
    to: email,
    subject: subject,
    text: text,
    html: html,
  };
  const data = await transporter.sendMail(mailData);
  return data;
}

module.exports = {
  sendEmailRegistration,
  sendEmailCorrectValidation,
  sendEmail,
};
