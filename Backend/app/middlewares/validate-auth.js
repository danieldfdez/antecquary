"use strict";

const jwt = require("jsonwebtoken");
const createJsonError = require("../errors/create-json-error");
const { JWT_SECRET } = process.env;

function extractAccessToken(headers) {
  const { authorization } = headers;
  if (!authorization || !authorization.startsWith("Bearer ")) {
    const error = new Error("Requiere de autorización para acceder");
    error.status = 403;
    throw error;
  }
  return authorization.split(" ")[1];
}

function validateAuth(req, res, next) {
  try {
    const token = extractAccessToken(req.headers);
    const decodedToken = jwt.verify(token, JWT_SECRET);
    const {
      id,
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      phone,
      nickname,
      email,
      rol,
      image,
      verifiedAt,
    } = decodedToken;
    req.auth = {
      id,
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      phone,
      nickname,
      email,
      rol,
      image,
      verifiedAt,
    };
    next();
  } catch (error) {
    error.status = 401;
    createJsonError(error, res);
  }
}

module.exports = validateAuth;
