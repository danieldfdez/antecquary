"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const { isAdmin } = require("../../helpers/utils");
const { updatePolitic } = require("../../repositories/politics-repository");

const schema = Joi.object({
  newExpeditionDate: Joi.date().required(),
  content: Joi.string().required(),
  expeditionDate: Joi.date().required(),
});

async function updatePoliticByDate(req, res) {
  try {
    const { rol } = req.auth;
    isAdmin(rol);
    const { body } = req;
    await schema.validateAsync(body);
    const { newExpeditionDate, content, expeditionDate } = body;
    const politic = {
      newExpeditionDate,
      content,
      expeditionDate,
    };
    await updatePolitic(politic);
    res.status(204);
    res.send(politic);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { updatePoliticByDate };
