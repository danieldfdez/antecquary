"use strict";

const createJsonError = require("../../errors/create-json-error");
const { isAdmin } = require("../../helpers/utils");
const { getAllPolitics } = require("../../repositories/politics-repository");

async function getPolitics(req, res) {
  try {
    const politics = await getAllPolitics();
    res.status(200);
    res.send(politics);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getPolitics };
