"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const { isAdmin } = require("../../helpers/utils");
const { addPolitic } = require("../../repositories/politics-repository");

const schema = Joi.object({
  expeditionDate: Joi.date().required(),
  content: Joi.string().required(),
});

async function createPolitic(req, res) {
  try {
    const { rol } = req.auth;
    isAdmin(rol);
    const { body } = req;
    await schema.validateAsync(body);
    const { expeditionDate, content } = body;
    const politic = {
      expeditionDate,
      content,
    };
    await addPolitic(politic);
    res.status(200);
    res.send();
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { createPolitic };
