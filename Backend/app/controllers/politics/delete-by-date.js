"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const { isAdmin } = require("../../helpers/utils");
const { removePolitic } = require("../../repositories/politics-repository");

const schema = Joi.object({
  expeditionDate: Joi.date().required(),
});

async function deletePoliticByDate(req, res) {
  try {
    const { rol } = req.auth;
    isAdmin(rol);
    const { body } = req;
    await schema.validateAsync(body);
    const { expeditionDate } = body;
    await removePolitic(expeditionDate);
    res.status(204);
    res.send();
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { deletePoliticByDate };
