"use strict";

const Joi = require("joi");
const bcrypt = require("bcryptjs");
const cryptoRandomString = require("crypto-random-string");
const {
  getUserByEmail,
  getUserByNickname,
  updateUserById,
  findUserById,
  addVerificationCode,
} = require("../../repositories/users-repository");
const createJsonError = require("../../errors/create-json-error");
const { sendEmailRegistration, sendEmail } = require("../../helpers/mail-smtp");
const throwJsonError = require("../../errors/throw-json-error");

const schema = Joi.object().keys({
  firstName: Joi.string().min(1).max(240).required(),
  lastName: Joi.string().min(1).max(240).required(),
  birthday: Joi.date().required(),
  sex: Joi.string().valid("hombre", "mujer", "no especificar"),
  NIF: Joi.string().min(8).max(20).required(),
  country: Joi.string().min(1).max(240).required(),
  city: Joi.string().min(1).max(240).required(),
  adress: Joi.string().min(1).max(240).required(),
  CP: Joi.number().integer().positive().required(),
  phone: Joi.number().integer().positive(),
  nickname: Joi.string().min(1).max(20).required(),
  email: Joi.string().required(),
});

const schemaAdmin = Joi.object().keys({
  newRolUser: Joi.string().valid("admin", "reader"),
  subject: Joi.string().required(),
  text: Joi.string().required(),
  html: Joi.string().required(),
});

const schemaPassword = Joi.object().keys({
  password: Joi.string().min(4).max(20).required(),
  repeatPassword: Joi.ref("password"),
});

async function updateUser(req, res) {
  try {
    const { id, rol } = req.auth;
    const {
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      phone,
      nickname,
      email,
      password,
      repeatPassword,
    } = req.body[0];
    await schema.validateAsync({
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      phone,
      nickname,
      email,
    });

    const userById = await findUserById(id);
    const user = await getUserByEmail(email);

    const nick = await getUserByNickname(nickname);
    if (nick && nick.id !== id) {
      const error = new Error(
        "Ya existe un usuario registrado con ese nombre!"
      );
      error.status = 409;
      throw error;
    }

    if (user && user.id !== id) {
      throwJsonError("Ya existe un usuario con ese email", 409);
    }

    if (email !== userById.email) {
      const verificationCode = cryptoRandomString({ length: 64 });
      await sendEmailRegistration(nickname, email, verificationCode);
      await addVerificationCode(id, verificationCode);
    }
    let updatedPassword = userById.password;
    if (password) {
      await schemaPassword.validateAsync({ password, repeatPassword });
      const passwordHash = await bcrypt.hash(password, 12);

      updatedPassword = passwordHash;
    }

    if (rol === "admin") {
      const { newRolUser, subject, text, html } = req.body[1];
      await schemaAdmin.validateAsync({ newRolUser, subject, text, html });
      sendEmail(email, subject, text, html);
      console.log(userId);
      await updateUserById({
        id: userId,
        firstName,
        lastName,
        birthday,
        sex,
        NIF,
        country,
        city,
        adress,
        CP,
        phone,
        nickname,
        email,
        password: updatedPassword,
        rol: newRolUser,
      });
      const userRol = (await getUserByEmail(email)).rol;

      res.status(204);
      res.send({
        id: userId,
        firstName,
        lastName,
        birthday,
        sex,
        NIF,
        country,
        city,
        adress,
        CP,
        phone,
        nickname,
        email,
        rol: userRol,
      });
    } else {
      await updateUserById({
        id,
        firstName,
        lastName,
        birthday,
        sex,
        NIF,
        country,
        city,
        adress,
        CP,
        phone,
        nickname,
        email,
        password: updatedPassword,
        rol: userById.rol,
      });

      res.status(200);
      res.send({
        id,
        firstName,
        lastName,
        birthday,
        sex,
        NIF,
        country,
        city,
        adress,
        CP,
        phone,
        nickname,
        email,
        rol: userById.rol,
      });
    }
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { updateUser };
