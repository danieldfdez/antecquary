"use strict";

const Joi = require("joi");
const cryptoRandomString = require("crypto-random-string");
const createJsonError = require("../../errors/create-json-error");
const path = require("path");
const fs = require("fs");
const {
  //getUserByEmail,
  findUserProfileImage,
  uploadUserProfileImage,
} = require("../../repositories/users-repository");
const throwJsonError = require("../../errors/throw-json-error");
//const { sendEmail } = require("../../helpers/mail-smtp");
const validExtensions = [".jpeg", ".jpg", ".png"];

const schemaAdmin = Joi.object().keys({
  email: Joi.string().required(),
  subject: Joi.string().required(),
  text: Joi.string().required(),
  html: Joi.string().required(),
});

async function uploadImageProfile(req, res) {
  try {
    const { id, rol } = req.auth;
    const { files, body } = req;
    // const { email, subject, text, html } = body;
    if (!files || Object.keys(files).length === 0) {
      throwJsonError("No se ha seleccionado ningún fichero", 400);
    }
    const { profileImage } = files;
    const extension = path.extname(profileImage.name);

    if (!validExtensions.includes(extension)) {
      throwJsonError("Formato no valido", 400);
    }

    // if (rol === "admin") {
    //   const userProfile = await getUserByEmail(email);
    //   const userImage = await findUserProfileImage(userProfile.id);
    //   const pathUserProfileImageFolder = `${__dirname}/../../../public/${PATH_USER_IMAGE}`;
    //   console.log(pathUserProfileImageFolder);
    //   if (userImage.image) {
    //     await fs.unlink(
    //       `${pathUserProfileImageFolder}/${userImage.image}`,
    //       () => {
    //         console.log("Borrada imagen de perfil correctamente");
    //       }
    //     );
    //   }
    //   const userImageName = `${userProfile.id}-${random}${extension}`;
    //   profileImage.mv(pathImage, async function (err) {
    //     if (err) return res.status(500).send(err);
    //     await uploadUserProfileImage(userProfile.id, userImageName);
    //     res.send({
    //       url: `${HTTP_SERVER_DOMAIN}/${PATH_USER_IMAGE}/${userImageName}`,
    //     });
    //   });
    //   await schemaAdmin.validateAsync(body);
    //   sendEmail(email, subject, text, html);
    // } else {
    const { PATH_USER_IMAGE } = process.env;
    const user = await findUserProfileImage(id);

    const pathProfileImageFolder = `${__dirname}/../../../../Frontend/public/${PATH_USER_IMAGE}`;
    if (user.image) {
      await fs.unlink(`${pathProfileImageFolder}/${user.image}`, () => {
        console.log("Borrada imagen de perfil correctamente");
      });
    }
    const random = cryptoRandomString({ length: 10, type: "alphanumeric" });
    const imageName = `${id}-${random}${extension}`;
    const pathImage = `${pathProfileImageFolder}/${imageName}`;
    profileImage.mv(pathImage, async function (err) {
      if (err) return res.status(500).send(err);
      await uploadUserProfileImage(id, imageName);
      res.send({
        url: `${imageName}`,
      });
    });
    // }
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { uploadImageProfile };
