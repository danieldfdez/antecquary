"use strict";

const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { findAllUsers } = require("../../repositories/users-repository");

async function getUsers(req, res) {
  try {
    const { rol } = req.auth;
    if (rol !== "admin") {
      throwJsonError("No tienes permisos", 401);
    } else {
      const users = await findAllUsers();
      res.status(200);
      res.send(users);
    }
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getUsers };
