"use strict";
const Joi = require("joi");
const bcrypt = require("bcryptjs");
const cryptoRandomString = require("crypto-random-string");

const createJsonError = require("../../errors/create-json-error");
const { sendEmailRegistration } = require("../../helpers/mail-smtp");
const {
  addUser,
  getUserByEmail,
  getUserByNickname,
} = require("../../repositories/users-repository");

const schema = Joi.object({
  firstName: Joi.string().min(1).max(240).required(),
  lastName: Joi.string().min(1).max(240).required(),
  birthday: Joi.date().required(),
  sex: Joi.string().valid("hombre", "mujer", "no especificar"),
  NIF: Joi.string().min(1).max(20).required(),
  country: Joi.string().min(1).max(240).required(),
  city: Joi.string().min(1).max(240).required(),
  adress: Joi.string().min(1).max(240).required(),
  CP: Joi.number().integer().positive().required(),
  phone: Joi.number().integer().positive(),
  nickname: Joi.string().min(1).max(20).required(),
  email: Joi.string().required(),
  password: Joi.string().min(1).max(20).required(),
  repeatPassword: Joi.ref("password"),
});

async function registerUser(req, res) {
  try {
    const { body } = req;
    await schema.validateAsync(body);
    const {
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      phone,
      nickname,
      email,
      password,
    } = body;
    const user = await getUserByEmail(email);
    if (user) {
      const error = new Error("Ya existe un usuario registrado con ese email!");
      error.status = 409;
      console.log(error);
      throw error;
    }
    const nick = await getUserByNickname(nickname);
    if (nick) {
      const error = new Error(
        "Ya existe un usuario registrado con ese nombre!"
      );
      error.status = 409;
      throw error;
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const verificationCode = await cryptoRandomString({ length: 64 });
    const userDB = {
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      phone,
      nickname,
      email,
      passwordHash,
      verificationCode,
    };
    const userId = await addUser(userDB);
    await sendEmailRegistration(nickname, email, verificationCode);

    res.status(200);
    res.send({ userId });
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { registerUser };
