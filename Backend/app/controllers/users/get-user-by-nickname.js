"use strict";

const createJsonError = require("../../errors/create-json-error");
const { getUserByNickname } = require("../../repositories/users-repository");

async function findUserByNickname(req, res) {
  try {
    const { nickname } = req.params;
    const user = await getUserByNickname(nickname);
    res.status(200);
    res.send(user);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { findUserByNickname };
