"use strict";
const createJsonError = require("../../errors/create-json-error");
const {
  activateValidation,
  getUserByVerificationCode,
} = require("../../repositories/users-repository");
const { sendEmailCorrectValidation } = require("../../helpers/mail-smtp");

async function activateUser(req, res) {
  try {
    const { verification_code: verificationCode } = req.query;

    if (!verificationCode) {
      return res.status(400).json({
        message: "Código de verificación no válido",
      });
    }

    const isActivated = await activateValidation(verificationCode);

    if (!isActivated) {
      res.send({
        message: "Cuenta no activada, código de verificación caducado.",
      });
    }

    const user = await getUserByVerificationCode(verificationCode);
    const { nickname, email } = user;
    await sendEmailCorrectValidation(nickname, email);

    res.send({ message: "Cuenta activada" });
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { activateUser };
