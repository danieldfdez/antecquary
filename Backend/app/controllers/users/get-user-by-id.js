"use strict";

const createJsonError = require("../../errors/create-json-error");
const { findUserById } = require("../../repositories/users-repository");

async function getUserById(req, res) {
  try {
    const { idUser } = req.params;
    const user = await findUserById(idUser);
    res.status(200);
    res.send(user);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getUserById };
