"use strict";

const Joi = require("joi");
const {
  findUserById,
  getUserByEmail,
} = require("../../repositories/users-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

async function getUserProfile(req, res) {
  try {
    const { id, rol } = req.auth;

    if (rol === "admin") {
      const { body } = req;
      const { emailUser } = body;
      const userProfile = await getUserByEmail(emailUser);
      const image = `${userProfile.image}`;

      const {
        firstName,
        lastName,
        birthday,
        sex,
        NIF,
        country,
        city,
        adress,
        CP,
        phone,
        nickname,
        email,
        rol,
        verifiedAt,
      } = userProfile;

      res.status(200);
      res.send({
        id,
        firstName,
        lastName,
        birthday,
        sex,
        NIF,
        country,
        city,
        adress,
        CP,
        phone,
        nickname,
        email,
        rol,
        verifiedAt,
        image,
      });
    } else {
      const user = await findUserById(id);

      const image = `${user.image}`;

      const {
        firstName,
        lastName,
        birthday,
        sex,
        NIF,
        country,
        city,
        adress,
        CP,
        phone,
        nickname,
        email,
        rol,
        verifiedAt,
      } = user;

      res.status(200);
      res.send({
        id,
        firstName,
        lastName,
        birthday,
        sex,
        NIF,
        country,
        city,
        adress,
        CP,
        phone,
        nickname,
        email,
        rol,
        verifiedAt,
        image,
      });
    }
  } catch (err) {
    console.log(err);
    if (err.name === "jwt expired") {
      throwJsonError("La sesión ha caducado", 401);
    } else {
      createJsonError(err, res);
    }
  }
}

module.exports = { getUserProfile };
