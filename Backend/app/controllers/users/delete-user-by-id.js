"use strict";

const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const {
  findUserById,
  removeUserById,
} = require("../../repositories/users-repository");
const throwJsonError = require("../../errors/throw-json-error");
const { sendEmail } = require("../../helpers/mail-smtp");
const { deleteImageProfile } = require("../../helpers/utils");

const schema = Joi.number().positive();

const schemaAdmin = Joi.object().keys({
  subject: Joi.string().required(),
  text: Joi.string().required(),
  html: Joi.string().required(),
});

async function deleteUserById(req, res) {
  try {
    const { id, rol } = req.auth;
    const idRoute = req.params.id;
    await schema.validateAsync(idRoute);
    const user = await findUserById(idRoute);
    if (!user) {
      throwJsonError("Usuario no existe", 400);
    }
    if (rol === "admin") {
      const { subject, text, html } = req.body;
      await schemaAdmin.validateAsync({ subject, text, html });
      sendEmail(user.email, subject, text, html);
      await deleteImageProfile(user);
      await removeUserById(idRoute);
      res.status(204).send();
    } else if (`${id}` === idRoute) {
      await deleteImageProfile(user);
      await removeUserById(idRoute);
      res.status(204).send();
    } else {
      throwJsonError("No tienes permisos para realizar esa acción", 401);
    }
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { deleteUserById };
