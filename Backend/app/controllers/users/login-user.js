"use strict";

const Joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { login } = require("../../repositories/users-repository");
const createJsonError = require("../../errors/create-json-error");

const schema = Joi.object({
  username: Joi.alternatives()
    .try(Joi.string().email(), Joi.string().alphanum().min(3).max(30))
    .required(),
  password: Joi.string().min(4).max(20).required(),
});

async function loginUser(req, res) {
  try {
    const { body } = req;
    await schema.validateAsync(body);
    const { username, password } = body;
    const user = await login(username, password);
    if (!user) {
      const error = new Error(
        "El nickname, email y password no coinciden con ningun usuario registrado. Por favor, intentelo de nuevo."
      );
      error.status = 403;
      throw error;
    }
    const {
      id,
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      nickname,
      phone,
      email,
      rol,
      verifiedAt,
      image,
    } = user;
    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) {
      const error = new Error(
        "El nickname, email y password no coinciden con ningun usuario registrado. Por favor, intentelo de nuevo."
      );
      error.status = 403;
      throw error;
    }
    if (!verifiedAt) {
      const error = new Error(
        "Verifique su cuenta para poder acceder a nuestros servicios"
      );
      error.status = 401;
      throw error;
    }
    const { JWT_SECRET } = process.env;
    const tokenPayload = {
      id,
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      phone,
      nickname,
      email,
      rol,
      verifiedAt,
    };
    const token = jwt.sign(tokenPayload, JWT_SECRET);

    const response = {
      accessToken: token,
    };

    res.status(200);
    res.send(response);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { loginUser };
