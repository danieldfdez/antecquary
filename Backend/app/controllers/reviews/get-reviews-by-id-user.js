"use strict";

const {
  findReviewsByUserId,
} = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

async function getReviewsByUserId(req, res) {
  try {
    const { idUser } = req.params;
    const reviews = await findReviewsByUserId(idUser);
    if (reviews.length === 0) {
      throwJsonError("Este usuario no tiene reseñas", 400);
    }
    res.send(reviews);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getReviewsByUserId };
