"use strict";

const {
  findReviewsByIdReviewer,
} = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

async function getReviewsByIdReviewer(req, res) {
  try {
    const { rol } = req.auth;
    if (rol !== "admin") {
      throwJsonError("No tienes permisos para obtener esta infomación");
    }
    const { idReviewer } = req.params;
    const reviews = await findReviewsByIdReviewer(idReviewer);
    if (reviews.length === 0) {
      throwJsonError("Este usuario no ha hecho reseñas", 400);
    }
    res.send(reviews);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getReviewsByIdReviewer };
