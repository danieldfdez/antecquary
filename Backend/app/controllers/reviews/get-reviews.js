"use strict";

const Joi = require("joi");
const { findAllReviews } = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

async function getReviews(req, res) {
  try {
    const { rol } = req.auth;
    if (rol !== "admin") {
      throwJsonError("No tienes permisos para realizar esta acción", 403);
    }
    const reviews = await findAllReviews();
    res.send(reviews);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getReviews };
