"use strict";
const Joi = require("joi");
const {
  updateReview,
  findReviewById,
} = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

const schemaId = Joi.number().positive().required();

const schemaReview = Joi.object().keys({
  content: Joi.string().min(5).max(1000),
  rating: Joi.number().min(0).max(10).required(),
});

async function updateReviewByIdReview(req, res) {
  try {
    const { id } = req.auth;
    const { idReview } = req.params;
    await schemaReview.validateAsync(req.body);
    await schemaId.validateAsync(idReview);
    const { content, rating } = req.body;
    const { idReviewer } = await findReviewById(idReview);
    if (idReviewer !== id) {
      throwJsonError("No tienes permisos para realizar esa acción", 400);
    }
    await updateReview(idReview, content, rating);

    res.status(200);
    res.send({ idReview, content, rating });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { updateReviewByIdReview };
