"use strict";

const { getAverageRating } = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

async function getValorationByUserId(req, res) {
  try {
    const { idUser } = req.params;
    const valoration = await getAverageRating(idUser);
    if (!valoration) {
      throwJsonError("Este usuario no tiene reseñas", 400);
    } else {
      res.send(valoration);
    }
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getValorationByUserId };
