"use strict";

const {
  findReviewByProductIdAndIdReviewer,
} = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

async function getReviewByProductIdAndIdReviewer(req, res) {
  try {
    const { id } = req.auth;
    const { idProduct } = req.params;
    const review = await findReviewByProductIdAndIdReviewer(idProduct, id);
    console.log(review);
    if (!review) {
      throwJsonError("Esta reseña no existe", 400);
    }
    res.send(review);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getReviewByProductIdAndIdReviewer };
