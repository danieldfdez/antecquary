"use strict";

const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { findUserById } = require("../../repositories/users-repository");
const { sendEmail } = require("../../helpers/mail-smtp");
const {
  findReviewById,
  deleteReviewById,
} = require("../../repositories/reviews-repository");

const schemaAdmin = Joi.object().keys({
  subject: Joi.string().required(),
  text: Joi.string().required(),
  html: Joi.string().required(),
});

async function deleteReview(req, res) {
  try {
    const { id, rol } = req.auth;
    const { idReview } = req.params;

    const review = await findReviewById(idReview);
    if (!review) {
      throwJsonError("Esa reseña no existe", 400);
    }

    if (rol === "admin") {
      const { subject, text, html } = req.body;
      await schemaAdmin.validateAsync({ subject, text, html });
      const { email } = findUserById(review.idReviewer);
      sendEmail(email, subject, text, html);
      await deleteReviewById(idReview);
      res.status(200);
      res.send({ message: `Review id:${idReview} borrada` });
    } else if (review.idReviewer === id) {
      await deleteReviewById(idReview);
      res.status(200);
      res.send({ message: `Review id:${idReview} borrada` });
    } else {
      throwJsonError("No tienes permisos para realizar esta acción", 403);
    }
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { deleteReview };
