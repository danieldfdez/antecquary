"use strict";
const Joi = require("joi");
const { findTransfer } = require("../../repositories/transfers-repository");
const {
  findSelledProductById,
} = require("../../repositories/products-repository");
const {
  addReview,
  findReviewByProductIdAndIdReviewer,
} = require("../../repositories/reviews-repository");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");

const schemaId = Joi.number().positive().required();

const schemaReview = Joi.object().keys({
  idUser: Joi.string(),
  content: Joi.string().min(5).max(1000),
  rating: Joi.number().min(0).max(10),
});

async function createReview(req, res) {
  try {
    const { id } = req.auth;
    const { idProduct } = req.params;
    await schemaReview.validateAsync(req.body);
    await schemaId.validateAsync(idProduct);
    const { idUser, content, rating } = req.body;

    const product = await findSelledProductById(idProduct);
    if (!product) {
      throwJsonError("Producto no existe o aún en venta", 400);
    }

    const transfer = await findTransfer(id, idProduct);
    if (!transfer) {
      throwJsonError(
        "Solo puedes realizar una reseña si has adquirido ese producto de ese usuario",
        400
      );
    }
    const getReview = await findReviewByProductIdAndIdReviewer(idProduct, id);
    if (getReview) {
      throwJsonError("Ya has hecho una review de este producto", 400);
    }
    const idReview = await addReview(id, idUser, idProduct, content, rating);

    res.status(200);
    res.send({ idReview, idProduct, content, rating });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { createReview };
