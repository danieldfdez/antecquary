"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findImagesProductById,
} = require("../../repositories/image-products-repository");

const schema = Joi.number().integer().positive().required();

async function getImagesProductById(req, res) {
  try {
    const { idProduct } = req.params;
    await schema.validateAsync(idProduct);
    const product = await findImagesProductById(idProduct);
    if (product.length === 0) {
      throwJsonError("Este producto no tiene imagenes", 400);
    }
    res.status(200);
    res.send(product);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getImagesProductById };
