"use strict";

const Joi = require("joi");
const cryptoRandomString = require("crypto-random-string");
const createJsonError = require("../../errors/create-json-error");
const path = require("path");
const fs = require("fs");
const {
  uploadProductImage,
} = require("../../repositories/image-products-repository");
const throwJsonError = require("../../errors/throw-json-error");
const validExtensions = [".jpeg", ".jpg", ".png"];

const schema = Joi.number().positive();

const schemaAdmin = Joi.object().keys({
  email: Joi.string().required(),
  subject: Joi.string().required(),
  text: Joi.string().required(),
  html: Joi.string().required(),
});

async function uploadImageProduct(req, res) {
  try {
    const { id } = req.auth;
    const { files } = req;
    const { idProduct } = req.params;
    await schema.validateAsync(idProduct);
    if (!files || Object.keys(files).length === 0) {
      throwJsonError("No se ha seleccionado ningún fichero", 400);
    }
    const { productImage } = files;
    const extension = path.extname(productImage.name);

    if (!validExtensions.includes(extension)) {
      throwJsonError("Formato no valido", 400);
    }

    const { PATH_PRODUCTS_IMAGE } = process.env;

    const pathProductImageFolder = `${__dirname}/../../../../Frontend/public/${PATH_PRODUCTS_IMAGE}`;
    const random = cryptoRandomString({ length: 10, type: "alphanumeric" });
    const imageName = `${id}-${idProduct}-${random}${extension}`;
    const pathImage = `${pathProductImageFolder}/${imageName}`;
    productImage.mv(pathImage, async function (err) {
      if (err) return res.status(500).send(err);
      await uploadProductImage(idProduct, imageName);
      res.send({
        url: `${imageName}`,
      });
    });
    // }
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { uploadImageProduct };
