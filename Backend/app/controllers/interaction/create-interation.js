"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const { addInteraction } = require("../../repositories/interaction-repository");

const schema = Joi.object({
  idProduct: Joi.number().integer().positive().required(),
  favorite: Joi.string().valid("YES", "NOT"),
});

async function createInteraction(req, res) {
  try {
    const { id } = req.auth;
    const { body } = req;
    await schema.validateAsync(body);
    const { idProduct, favorite } = body;
    const userInteraction = {
      idUser: id,
      idProduct,
      favorite,
    };
    const interaction = await addInteraction(userInteraction);
    res.status(200);
    res.send(interaction);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { createInteraction };
