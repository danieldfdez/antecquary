"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const {
  removeInteractions,
} = require("../../repositories/interaction-repository");

const schema = Joi.object({
  idProduct: Joi.number().integer().positive().required(),
});

async function deleteInteractionsByIdProduct(req, res) {
  try {
    const { id } = req.auth;
    const { body } = req;
    await schema.validateAsync(body);
    const { idProduct } = body;
    await removeInteractions(id, idProduct);
    res.status(204);
    res.send();
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { deleteInteractionsByIdProduct };
