"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  getFavorite,
  removeInteractions,
} = require("../../repositories/interaction-repository");

const schema = Joi.object({
  idProduct: Joi.number().integer().positive().required(),
});

async function deleteFavoriteByIdProduct(req, res) {
  try {
    const { id } = req.auth;
    const { body } = req;
    await schema.validateAsync(body);
    const { idProduct } = body;
    const favorite = await getFavorite(id, idProduct);
    if (favorite[0].length !== 0) {
      await removeInteractions(id, idProduct);
      res.status(204);
      res.send();
    } else {
      throwJsonError("Este articulo no esta entre los favoritos", 400);
    }
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { deleteFavoriteByIdProduct };
