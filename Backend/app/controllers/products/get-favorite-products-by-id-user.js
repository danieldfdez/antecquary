"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findFavoriteProductsByIdUser,
} = require("../../repositories/products-repository");

async function getFavoriteProductsByIdUser(req, res) {
  try {
    const { id } = req.auth;
    const products = await findFavoriteProductsByIdUser(id);
    if (products.length === 0) {
      throwJsonError("Este usuario no tiene productos favoritos", 400);
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getFavoriteProductsByIdUser };
