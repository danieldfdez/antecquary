"use strict";

const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findProductById,
  updateProduct,
} = require("../../repositories/products-repository");
const { findUserById } = require("../../repositories/users-repository");
const { sendEmail } = require("../../helpers/mail-smtp");

const schemaId = Joi.number().positive().required();

const schemaProduct = Joi.object().keys({
  productName: Joi.string().min(3).max(30).required(),
  price: Joi.number().integer().positive().required(),
  city: Joi.string().min(3).max(30).required(),
  brand: Joi.string().min(3).max(30).required(),
  yearOfLaunch: Joi.number()
    .integer()
    .positive()
    .min(1900)
    .max(new Date().getFullYear()),
  yearOfPurchase: Joi.number()
    .integer()
    .positive()
    .min(1900)
    .max(new Date().getFullYear()),
  category: Joi.string().valid(
    "videojuegos",
    "ordenadores",
    "camaras",
    "telefonia",
    "audiovisual",
    "otros"
  ),
  actualStatus: Joi.string().valid(
    "malo",
    "defectuoso",
    "muy usado",
    "desgastado",
    "funcional",
    "bueno",
    "perfecto",
    "sin uso"
  ),
  originalDocuments: Joi.string().valid(
    "no tiene",
    "en mal estado",
    "en buen estado",
    "faltan partes",
    "completa",
    "impecable"
  ),
  originalPackage: Joi.string().valid(
    "no tiene",
    "en mal estado",
    "en buen estado",
    "impecable"
  ),
  accesories: Joi.string().min(3).max(255),
  descriptionProduct: Joi.string().min(3).max(1000).required(),
});

const schemaAdmin = Joi.object().keys({
  subject: Joi.string().required(),
  text: Joi.string().required(),
  html: Joi.string().required(),
});

async function updateProductById(req, res) {
  try {
    const { id, rol } = req.auth;
    const { idProduct } = req.params;
    const { body } = req;
    await schemaId.validateAsync(idProduct);
    const product = await findProductById(idProduct);
    if (!product) {
      throwJsonError("Producto no existe", 400);
    }
    const productToUpdate = body[0];
    const idUser = product.idSeller;
    const user = await findUserById(idUser);
    await schemaProduct.validateAsync(productToUpdate);

    if (rol === "admin") {
      const { subject, text, html } = body[1];
      await schemaAdmin.validateAsync({ subject, text, html });
      const email = user.email;
      sendEmail(email, subject, text, html);
      await updateProduct(idProduct, productToUpdate);
      const productUpdated = await findProductById(idProduct);
      const {
        productName,
        price,
        city,
        brand,
        yearOfLaunch,
        yearOfPurchase,
        category,
        actualStatus,
        originalDocuments,
        originalPackage,
        accesories,
        descriptionProduct,
      } = productUpdated;

      res.status(200);
      res.send({
        productName,
        price,
        city,
        brand,
        yearOfLaunch,
        yearOfPurchase,
        category,
        actualStatus,
        originalDocuments,
        originalPackage,
        accesories,
        descriptionProduct,
      });
    } else if (id === idUser) {
      await updateProduct(idProduct, productToUpdate);
      const productUpdated = await findProductById(idProduct);
      const {
        productName,
        price,
        city,
        brand,
        yearOfLaunch,
        yearOfPurchase,
        category,
        actualStatus,
        originalDocuments,
        originalPackage,
        accesories,
        descriptionProduct,
      } = productUpdated;
      res.status(200);
      res.send({
        productName,
        price,
        city,
        brand,
        yearOfLaunch,
        yearOfPurchase,
        category,
        actualStatus,
        originalDocuments,
        originalPackage,
        accesories,
        descriptionProduct,
      });
    } else {
      throwJsonError("No tienes autorización para realizar los cambios", 401);
    }
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { updateProductById };
