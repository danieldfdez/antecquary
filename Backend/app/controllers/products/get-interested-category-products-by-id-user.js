"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findInterestedCategoryProductsByIdUser,
} = require("../../repositories/products-repository");

async function getInterestedCategoryProductsByIdUser(req, res) {
  try {
    const { id } = req.auth;
    const products = await findInterestedCategoryProductsByIdUser(id);
    if (products.length === 0) {
      throwJsonError(
        "Este usuario no ha interactuado con ningún producto",
        400
      );
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getInterestedCategoryProductsByIdUser };
