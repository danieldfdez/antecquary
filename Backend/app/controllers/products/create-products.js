"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const { addProduct } = require("../../repositories/products-repository");

const schema = Joi.object().keys({
  productName: Joi.string().min(3).max(30).required(),
  price: Joi.number().integer().positive().required(),
  city: Joi.string().min(3).max(30).required(),
  brand: Joi.string().min(3).max(30).required(),
  yearOfLaunch: Joi.number().positive().min(1900).max(new Date().getFullYear()),
  yearOfPurchase: Joi.number()
    .integer()
    .positive()
    .min(1900)
    .max(new Date().getFullYear()),
  category: Joi.string().valid(
    "videojuegos",
    "ordenadores",
    "camaras",
    "telefonia",
    "audiovisual",
    "otros"
  ),
  actualStatus: Joi.string().valid(
    "malo",
    "defectuoso",
    "muy usado",
    "desgastado",
    "funcional",
    "bueno",
    "perfecto",
    "sin uso"
  ),
  originalDocuments: Joi.string().valid(
    "no tiene",
    "en mal estado",
    "en buen estado",
    "faltan partes",
    "completa",
    "impecable"
  ),
  originalPackage: Joi.string().valid(
    "no tiene",
    "en mal estado",
    "en buen estado",
    "impecable"
  ),
  accesories: Joi.string().min(3).max(255),
  descriptionProduct: Joi.string().min(3).max(1000).required(),
});

async function createProduct(req, res) {
  try {
    const { id } = req.auth;
    const { body } = req;
    await schema.validateAsync(body);

    await addProduct(id, body);
    res.status(201);
    res.send({});
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { createProduct };
