"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findVisitedProductsByIdUser,
} = require("../../repositories/products-repository");

async function getVisitedProductsByIdUser(req, res) {
  try {
    const { id } = req.auth;
    const products = await findVisitedProductsByIdUser(id);
    if (products.length === 0) {
      throwJsonError(
        "Este usuario no ha interactuado con ningún producto",
        400
      );
    } else {
      res.status(200);
      res.send(products);
    }
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getVisitedProductsByIdUser };
