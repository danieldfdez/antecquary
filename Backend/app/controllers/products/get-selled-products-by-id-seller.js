"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findSelledProductsByIdSeller,
} = require("../../repositories/products-repository");

async function getSelledProductsByIdSeller(req, res) {
  try {
    const { id } = req.auth;
    const products = await findSelledProductsByIdSeller(id);
    if (products.length === 0) {
      throwJsonError("Este usuario no tiene productos vendidos", 400);
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getSelledProductsByIdSeller };
