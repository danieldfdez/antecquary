"use strict";

const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findProductsByPrice,
} = require("../../repositories/products-repository");

const schema = Joi.number().integer().positive().required();

async function getProductsByPrice(req, res) {
  try {
    const { price } = req.params;
    await schema.validateAsync(price);
    const products = await findProductsByPrice(price);
    if (products.length === 0) {
      throwJsonError("No hay productos publicados en ese rango de precio", 400);
    }

    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getProductsByPrice };
