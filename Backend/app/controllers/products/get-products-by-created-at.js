"use strict";

const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findProductsByCreatedAt,
} = require("../../repositories/products-repository");

async function getProductsByCreatedAt(req, res) {
  try {
    const { createdAtProducts } = req.params;
    const products = await findProductsByCreatedAt(createdAtProducts);
    if (products.length === 0) {
      throwJsonError("No hay Productos publicados recientemente", 400);
    }

    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getProductsByCreatedAt };
