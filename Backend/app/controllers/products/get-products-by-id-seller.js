"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findProductsByIdSeller,
} = require("../../repositories/products-repository");

async function getProductsByIdSeller(req, res) {
  try {
    const { idSeller } = req.params;
    const products = await findProductsByIdSeller(idSeller);
    if (products.length === 0) {
      throwJsonError("Este usuario no tiene productos", 400);
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getProductsByIdSeller };
