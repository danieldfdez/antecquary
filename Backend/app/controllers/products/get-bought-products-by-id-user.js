"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findBoughtProductsByIdUser,
} = require("../../repositories/products-repository");

async function getBoughtProductsByIdUser(req, res) {
  try {
    const { id } = req.auth;
    const products = await findBoughtProductsByIdUser(id);
    if (products.length === 0) {
      throwJsonError("Este usuario no tiene productos comprados", 400);
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getBoughtProductsByIdUser };
