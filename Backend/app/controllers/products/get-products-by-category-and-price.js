"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findProductsByCategoryAndPrice,
} = require("../../repositories/products-repository");

const schema = Joi.object().keys({
  category: Joi.string().valid(
    "videojuegos",
    "ordenadores",
    "camaras",
    "telefonia",
    "audiovisual",
    "sin especificar"
  ),
  price: Joi.number().integer().positive().required(),
});

async function getProductsByCategoryAndPrice(req, res) {
  try {
    const { params } = req;
    await schema.validateAsync(params);
    const { category, price } = req.params;
    const products = await findProductsByCategoryAndPrice(category, price);
    if (products.length === 0) {
      throwJsonError(
        "No hay productos en esa categoria con ese rango de precio",
        400
      );
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getProductsByCategoryAndPrice };
