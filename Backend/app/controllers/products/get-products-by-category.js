"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findProductsByCategory,
} = require("../../repositories/products-repository");

const schema = Joi.string().valid(
  "videojuegos",
  "ordenadores",
  "camaras",
  "telefonia",
  "audiovisual",
  "sin especificar"
);

async function getProductsByCategory(req, res) {
  try {
    const { category } = req.params;
    await schema.validateAsync(category);
    const products = await findProductsByCategory(category);
    if (products.length === 0) {
      throwJsonError("No hay productos en esa categoria", 400);
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getProductsByCategory };
