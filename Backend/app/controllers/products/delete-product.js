"use strict";

const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findProductById,
  removeProductById,
} = require("../../repositories/products-repository");
const { findUserById } = require("../../repositories/users-repository");
const { sendEmail } = require("../../helpers/mail-smtp");

const schema = Joi.number().integer().positive().required();

const schemaAdmin = Joi.object().keys({
  subject: Joi.string().required(),
  text: Joi.string().required(),
  html: Joi.string().required(),
});

async function deleteProductById(req, res) {
  try {
    const { id, rol } = req.auth;
    const { idProduct } = req.params;
    await schema.validateAsync(idProduct);
    const product = await findProductById(idProduct);
    if (!product) {
      throw new Error("Producto no existe");
    }
    const idUser = product.idSeller;
    const user = await findUserById(idUser);

    if (rol === "admin") {
      const { body } = req;
      const { subject, text, html } = body;
      await schemaAdmin.validateAsync({ subject, text, html });
      const email = user.email;
      sendEmail(email, subject, text, html);
      await removeProductById(idProduct);

      res.status(200);
      res.send({
        message: `Producto numero:${idProduct} borrado correctamente!`,
      });
    } else if (id === idUser) {
      await removeProductById(idProduct);

      res.status(200);
      res.send({
        message: `Producto numero:${idProduct} borrado correctamente!`,
      });
    } else {
      throwJsonError("No tienes autorización para realizar esta acción", 401);
    }
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { deleteProductById };
