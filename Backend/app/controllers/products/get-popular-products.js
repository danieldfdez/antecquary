"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findPopularProducts,
} = require("../../repositories/products-repository");

async function getPopularProducts(req, res) {
  try {
    const products = await findPopularProducts();
    if (products.length === 0) {
      throwJsonError("No hay productos con interacciones", 400);
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getPopularProducts };
