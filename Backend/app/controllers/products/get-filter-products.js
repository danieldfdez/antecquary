"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { filterProducts } = require("../../repositories/products-repository");

async function getFilterProducts(req, res) {
  try {
    const { productName, price, city, category } = req.body;
    const products = await filterProducts(productName, price, city, category);
    if (products.length === 0) {
      throwJsonError("No hay ningún producto que cumpla esos requisitos", 400);
    }
    res.status(200);
    res.send(products);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getFilterProducts };
