"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { findProductById } = require("../../repositories/products-repository");

const schema = Joi.number().integer().positive().required();

async function getProductById(req, res) {
  try {
    const { idProduct } = req.params;
    await schema.validateAsync(idProduct);
    const product = await findProductById(idProduct);
    if (!product) {
      throwJsonError("Producto no existe", 400);
    }
    res.status(200);
    res.send(product);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getProductById };
