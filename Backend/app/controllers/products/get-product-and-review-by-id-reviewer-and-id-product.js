"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findProductAndReviewByIdReviewerAndIdProduct,
} = require("../../repositories/products-repository");

async function getProductAndReviewByIdReviewerAndIdProduct(req, res) {
  try {
    const { id } = req.auth;
    const { idProduct } = req.params;
    const product = await findProductAndReviewByIdReviewerAndIdProduct(
      id,
      idProduct
    );
    if (!product) {
      throwJsonError("No has hecho una reseña de este producto", 400);
    }
    res.status(200);
    res.send(product);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getProductAndReviewByIdReviewerAndIdProduct };
