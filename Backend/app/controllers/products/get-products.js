"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { findAllProducts } = require("../../repositories/products-repository");

async function getProducts(req, res) {
  try {
    const { rol } = req.auth;
    if (rol !== "admin") {
      throwJsonError("No tienes permisos", 401);
    } else {
      const products = await findAllProducts();
      res.status(200);
      res.send(products);
    }
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = {
  getProducts,
};
