"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { addMessage } = require("../../repositories/messages-repository");
const { findChatById } = require("../../repositories/chats-repository");

const schemaId = Joi.number().integer().positive().required();

const schema = Joi.string().required();

async function createMessage(req, res) {
  try {
    const { id } = req.auth;
    const { idChat } = req.params;
    const { content } = req.body;
    await schemaId.validateAsync(idChat);
    await schema.validateAsync(content);
    const chat = findChatById(idChat);
    if (!chat) {
      throwJsonError("Este chat no existe o ha sido borrado", 400);
    }
    const message = await addMessage(id, idChat, content);
    res.status(200);
    res.send(message);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { createMessage };
