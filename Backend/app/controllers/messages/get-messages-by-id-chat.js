"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  findMessagesByIdChat,
} = require("../../repositories/messages-repository");
const {
  findChatById,
  findChatsByUserId,
} = require("../../repositories/chats-repository");

const schemaId = Joi.number().integer().positive().required();

async function getMessagesByIdChat(req, res) {
  try {
    const { id } = req.auth;
    const { idChat } = req.params;
    await schemaId.validateAsync(idChat);
    const chat = await findChatById(idChat);
    if (!chat) {
      throwJsonError(`Este chat no existe o ha sido borrado`, 400);
    }
    const userChats = await findChatsByUserId(id);
    if (userChats.find((userChat) => `${userChat.id}` === idChat)) {
      const messages = await findMessagesByIdChat(idChat);
      res.status(200);
      res.send(messages);
    } else {
      throwJsonError(`No tienes acceso a este chat`, 400);
    }
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getMessagesByIdChat };
