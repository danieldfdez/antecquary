"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { addChat } = require("../../repositories/chats-repository");
const { findProductById } = require("../../repositories/products-repository");

const schema = Joi.number().integer().positive().required();

async function createChat(req, res) {
  try {
    const { id } = req.auth;
    const { idProduct } = req.params;
    await schema.validateAsync(idProduct);
    const product = await findProductById(idProduct);
    if (!product) {
      throwJsonError("Este producto no existe o ya no está a la venta", 400);
    } else if (product.idSeller === id) {
      throwJsonError("Este producto es suyo", 400);
    } else {
      const idChat = await addChat(id, idProduct);
      res.status(200);
      res.send({ idChat });
    }
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { createChat };
