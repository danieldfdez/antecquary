"use strict";
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { findChatsByUserId } = require("../../repositories/chats-repository");

async function getChatsByUserId(req, res) {
  try {
    const { id } = req.auth;
    const chats = await findChatsByUserId(id);
    if (chats.length === 0) {
      throwJsonError("No tienes chats activos", 400);
    }
    res.status(200);
    res.send(chats);
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { getChatsByUserId };
