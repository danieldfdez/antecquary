"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const { logicalRemoveChat } = require("../../repositories/chats-repository");
const {
  findSelledProductById,
} = require("../../repositories/products-repository");

const schema = Joi.number().integer().positive().required();

async function logicalDeleteChatByIdProduct(req, res) {
  try {
    // const { id } = req.auth;
    const { idProduct } = req.params;
    await schema.validateAsync(idProduct);
    const product = await findSelledProductById(idProduct);
    if (!product) {
      throwJsonError("Este producto no existe o aún está a la venta", 400);
    } else {
      await logicalRemoveChat(idProduct);
      res.status(200);
      res.send({ message: "Chat borrado con éxito" });
    }
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { logicalDeleteChatByIdProduct };
