"use strict";
const createJsonError = require("../../errors/create-json-error");
const {
  findTransferByIdProduct,
} = require("../../repositories/transfers-repository");

async function getTransferByIdProduct(req, res) {
  try {
    const { idProduct } = req.params;
    const transfer = await findTransferByIdProduct(idProduct);
    res.status(200);
    res.send(transfer);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getTransferByIdProduct };
