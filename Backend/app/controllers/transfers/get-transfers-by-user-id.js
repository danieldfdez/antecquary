"use strict";
const createJsonError = require("../../errors/create-json-error");
const {
  getAllTransfersByUserId,
} = require("../../repositories/transfers-repository");

async function getTransfersByUserId(req, res) {
  try {
    const { id } = req.auth;
    const userTransfers = await getAllTransfersByUserId(id);
    res.status(200);
    res.send(userTransfers);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getTransfersByUserId };
