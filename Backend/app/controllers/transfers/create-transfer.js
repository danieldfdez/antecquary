"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const throwJsonError = require("../../errors/throw-json-error");
const {
  logicalRemoveProductById,
} = require("../../repositories/products-repository");
const { addTransfer } = require("../../repositories/transfers-repository");
// const { findProductById } = require("../../repositories/products-repository");

const schema = Joi.object().keys({
  idUser: Joi.number().integer().positive().required(),
  idProduct: Joi.number().integer().positive().required(),
});

async function createTransfer(req, res) {
  try {
    const { id } = req.auth;
    const { idUser, idProduct } = req.params;
    await schema.validateAsync({ idProduct, idUser });
    const userTransfer = {
      idUser,
      idProduct,
    };
    if (id === idUser) {
      throwJsonError("No puedes comprar tu propio producto", 400);
    }
    const transfer = await addTransfer(userTransfer);
    await logicalRemoveProductById(idProduct);
    res.status(200);
    res.send(transfer);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { createTransfer };
