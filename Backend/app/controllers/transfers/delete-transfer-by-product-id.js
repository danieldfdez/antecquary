"use strict";
const Joi = require("joi");
const createJsonError = require("../../errors/create-json-error");
const {
  removeTransferByProductId,
} = require("../../repositories/transfers-repository");

const schema = Joi.object({
  idProduct: Joi.number().integer().positive().required(),
});

async function deleteTransferByProductId(req, res) {
  try {
    const { id } = req.auth;
    const { body } = req;
    await schema.validateAsync(body);
    const { idProduct } = body;
    await removeTransferByProductId(id, idProduct);
    res.status(204);
    res.send();
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { deleteTransferByProductId };
