"use strict";

const database = require("../infrastructure/database");

async function addPolitic(politic) {
  const pool = await database.getPool();
  const consulta = `INSERT INTO politics(expeditionDate,content) VALUES (?, ?)`;
  const [created] = await pool.query(consulta, [...Object.values(politic)]);
  return created[0];
}

async function updatePolitic(politic) {
  const { newExpeditionDate, content, expeditionDate } = politic;
  const pool = await database.getPool();
  const consulta = `UPDATE politics
  SET 
      expeditionDate = ?,
      content = ?
  WHERE expeditionDate = ?`;
  await pool.query(consulta, [newExpeditionDate, content, expeditionDate]);
  return true;
}

async function removePolitic(date) {
  const pool = await database.getPool();
  const consulta = `DELETE FROM politics WHERE expeditionDate = ?`;
  await pool.query(consulta, date);

  return true;
}

async function getAllPolitics() {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM politics`;
  const [politics] = await pool.query(consulta);
  return politics;
}

module.exports = { addPolitic, updatePolitic, removePolitic, getAllPolitics };
