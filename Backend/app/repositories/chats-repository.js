"use strict";

const database = require("../infrastructure/database");

async function addChat(idClient, idProduct) {
  const pool = await database.getPool();
  const insertQuery = `INSERT
    INTO chats (idClient, idProduct)
    VALUES (?, ?)`;
  const [created] = await pool.query(insertQuery, [idClient, idProduct]);

  return created.insertId;
}

async function findChatById(idChat) {
  const pool = await database.getPool();
  const query = "SELECT * FROM chats WHERE id = ? AND deletedAt IS NULL";
  const [chat] = await pool.query(query, idChat);
  return chat[0];
}

async function findChatsByUserId(idUser) {
  const pool = await database.getPool();
  const query =
    "SELECT chats.*, products.idSeller, products.productName, products.city FROM chats LEFT JOIN products ON chats.idProduct = products.id WHERE (idClient = ? OR idSeller = ?) AND chats.deletedAt IS NULL";
  const [chats] = await pool.query(query, [idUser, idUser]);
  return chats;
}

async function logicalRemoveChat(idProduct) {
  const pool = await database.getPool();
  const now = new Date();
  const updateQuery = "UPDATE chats SET deletedAt = ? WHERE idProduct = ?";
  await pool.query(updateQuery, [now, idProduct]);
  return true;
}

module.exports = {
  addChat,
  findChatsByUserId,
  logicalRemoveChat,
  findChatById,
};
