"use strict";

const database = require("../infrastructure/database");

async function findTransfer(idUser, idProduct) {
  const pool = await database.getPool();
  const consulta = `
    SELECT * FROM transfers WHERE idUser = ? AND idProduct = ?
  `;
  const transfer = await pool.query(consulta, [idUser, idProduct]);
  return transfer;
}

async function findTransferByIdProduct(idProduct) {
  const pool = await database.getPool();
  const consulta = `
    SELECT * FROM transfers WHERE idProduct = ?
  `;
  const [transfer] = await pool.query(consulta, [idProduct]);
  return transfer[0];
}

async function addTransfer(user) {
  const pool = await database.getPool();
  const now = new Date();
  const consulta = `
    INSERT INTO transfers(
    idUser, idProduct, moment
    ) VALUES (?, ?, ?)
  `;
  const { idUser, idProduct } = user;
  const [created] = await pool.query(consulta, [idUser, idProduct, now]);
  return created;
}

async function removeTransferByProductId(idUser, idProduct) {
  const pool = await database.getPool();
  const consulta = `DELETE FROM transfers
    WHERE idUser = ? AND idProduct = ?`;
  await pool.query(consulta, [idUser, idProduct]);

  return true;
}

async function getAllTransfersByUserId(idUser) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM transfers WHERE idUser = ?`;
  const transfers = await pool.query(consulta, idUser);
  return transfers[0];
}

module.exports = {
  findTransfer,
  addTransfer,
  removeTransferByProductId,
  getAllTransfersByUserId,
  findTransferByIdProduct,
};
