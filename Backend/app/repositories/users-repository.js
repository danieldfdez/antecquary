"use strict";

const database = require("../infrastructure/database");

async function getUserByEmail(email) {
  const pool = await database.getPool();
  const query = `SELECT *
  FROM users
  WHERE email = ?`;
  const [user] = await pool.query(query, email);

  return user[0];
}

async function getUserByNickname(nick) {
  const pool = await database.getPool();
  const query = `SELECT *
  FROM users
  WHERE nickname = ?`;
  const [user] = await pool.query(query, nick);

  return user[0];
}

async function addUser(user) {
  const pool = await database.getPool();
  const now = new Date();
  const consulta = `
    INSERT INTO users(
    firstName,
    lastName,
    birthday,
    sex,
    NIF,
    country,
    city,
    adress,
    CP,
    phone,
    nickname,
    email,
    password,
    verificationCode,
    rol,
    createdAt
    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
  `;
  const [created] = await pool.query(consulta, [
    ...Object.values(user),
    "reader",
    now,
  ]);
  return created.insertId;
}

async function activateValidation(verificationCode) {
  const now = new Date();
  const pool = await database.getPool();
  const updateQuery = `UPDATE users
    SET verifiedAt = ?
    WHERE verificationCode = ?
    AND verifiedAt IS NULL`;

  const [resultActivation] = await pool.query(updateQuery, [
    now,
    verificationCode,
  ]);

  return resultActivation.affectedRows === 1;
}

async function getUserByVerificationCode(verificationCode) {
  const pool = await database.getPool();
  const consulta = `SELECT nickname, email
  FROM users
  WHERE verificationCode = ?`;
  const [user] = await pool.query(consulta, verificationCode);

  return user[0];
}

async function login(username) {
  const pool = await database.getPool();
  const consulta = `SELECT id, firstName, lastName, birthday, sex, NIF, country, city, adress, CP, phone, nickname, email, rol, verifiedAt, password, image
    FROM users
    WHERE email = ? OR nickname = ?`;
  const [user] = await pool.query(consulta, [username, username]);

  return user[0];
}

async function findUserById(id) {
  const pool = await database.getPool();
  const query = `SELECT * FROM users WHERE id = ?`;
  const [users] = await pool.query(query, id);

  return users[0];
}

async function updateUserById(data) {
  const now = new Date();
  const {
    id,
    firstName,
    lastName,
    birthday,
    sex,
    NIF,
    country,
    city,
    adress,
    CP,
    phone,
    nickname,
    email,
    password,
    rol,
  } = data;
  const pool = await database.getPool();
  const updateQuery = `UPDATE users
  SET 
      firstName = ?,
      lastName = ?,
      birthday = ?,
      sex = ?,
      NIF = ?,
      country = ?,
      city = ?,
      adress = ?,
      CP = ?,
      phone = ?,
      nickname = ?,
      email = ?,
      password = ?,
      rol = ?,
      updatedAt = ?
  WHERE id = ?`;
  await pool.query(updateQuery, [
    firstName,
    lastName,
    birthday,
    sex,
    NIF,
    country,
    city,
    adress,
    CP,
    phone,
    nickname,
    email,
    password,
    rol,
    now,
    id,
  ]);

  return true;
}

async function addVerificationCode(id, code) {
  const now = new Date();
  const pool = await database.getPool();
  const insertQuery = `
    UPDATE users SET verificationCode = ?,
    updatedAt = ?,
    verifiedAt = ?
    WHERE id = ?
  `;
  const [created] = await pool.query(insertQuery, [code, now, , id]);

  return created.insertId;
}

async function removeUserById(id) {
  const pool = await database.getPool();
  const consulta = "DELETE FROM users WHERE id = ?";
  await pool.query(consulta, id);

  return true;
}

async function findReviewsByUserId(idUser) {
  const pool = await database.getPool();
  const query = `SELECT * FROM reviews
    LEFT JOIN users ON users.id = reviews.idReviewer
    WHERE idReviewer = ?`;
  const [reviews] = await pool.query(query, idUser);

  return reviews;
}

async function findAllUsers() {
  const pool = await database.getPool();
  const consulta = "SELECT id, nickname, email, verifiedAt FROM users";
  const [users] = await pool.query(consulta);

  return users;
}

async function findUserProfileImage(id) {
  const pool = await database.getPool();
  const query = "SELECT image FROM users WHERE id = ?";
  const [users] = await pool.query(query, id);

  return users[0];
}

async function uploadUserProfileImage(id, image) {
  const pool = await database.getPool();
  const updateQuery = "UPDATE users SET image = ? WHERE id = ?";
  await pool.query(updateQuery, [image, id]);

  return true;
}

module.exports = {
  addUser,
  getUserByEmail,
  getUserByNickname,
  activateValidation,
  getUserByVerificationCode,
  login,
  findUserById,
  updateUserById,
  addVerificationCode,
  removeUserById,
  findReviewsByUserId,
  findAllUsers,
  findUserProfileImage,
  uploadUserProfileImage,
};
