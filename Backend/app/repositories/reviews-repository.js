"use strict";

const database = require("../infrastructure/database");

async function addReview(idReviewer, idUser, idProduct, content, rating) {
  const pool = await database.getPool();
  const now = new Date();
  const insertQuery = `INSERT
    INTO reviews (idUser, idProduct, idReviewer, content, rating, createdAt)
    VALUES (?, ?, ?, ?, ?, ?)`;
  const [created] = await pool.query(insertQuery, [
    idUser,
    idProduct,
    idReviewer,
    content,
    rating,
    now,
  ]);

  return created.insertId;
}

async function deleteReviewById(id) {
  const pool = await database.getPool();
  const query = "DELETE FROM reviews WHERE id = ?";
  const [reviews] = await pool.query(query, id);

  return reviews;
}

async function findReviewById(id) {
  const pool = await database.getPool();
  const query = "SELECT * FROM reviews WHERE id = ?";
  const [reviews] = await pool.query(query, id);

  return reviews[0];
}

async function findReviewByProductIdAndIdReviewer(idProduct, idReviewer) {
  const pool = await database.getPool();
  const query = "SELECT * FROM reviews WHERE idProduct = ? AND idReviewer = ?";
  const [reviews] = await pool.query(query, [idProduct, idReviewer]);

  return reviews[0];
}

async function findReviewsByUserId(idUser) {
  const pool = await database.getPool();
  const query = `SELECT reviews.*, products.productName, products.brand, products.yearOfLaunch
    FROM reviews
    LEFT JOIN products ON products.id = reviews.idproduct
    WHERE idUser = ?`;
  const [reviews] = await pool.query(query, idUser);

  return reviews;
}

async function findReviewsByIdReviewer(idReviewer) {
  const pool = await database.getPool();
  const query = "SELECT * FROM reviews WHERE idReviewer = ?";
  const [reviews] = await pool.query(query, idReviewer);

  return reviews;
}

async function findAllReviews() {
  const pool = await database.getPool();
  //const query = 'SELECT * FROM reviews';
  const query = `SELECT reviews.*, users.nombre, product.marca, cars.modelo, products.yearLaunch
    FROM reviews
    INNER JOIN users ON users.id = reviews.idUser
    INNER JOIN product ON product.id = idProduct`;
  const [reviews] = await pool.query(query);

  return reviews;
}

async function getAverageRating(idUser) {
  const pool = await database.getPool();
  const query =
    "SELECT AVG(rating) as valoracionMedia FROM reviews WHERE idUser = ?";
  const [valoracion] = await pool.query(query, idUser);
  console.log(valoracion[0].valoracionMedia);

  return valoracion.valoracionMedia;
}

async function updateReview(idReview, content, rating) {
  const pool = await database.getPool();
  const now = new Date();
  const updateQuery =
    "UPDATE reviews SET content = ?, rating = ?, updatedAt = ? WHERE id = ?";
  await pool.query(updateQuery, [content, rating, now, idReview]);

  return true;
}

module.exports = {
  addReview,
  deleteReviewById,
  findAllReviews,
  findReviewById,
  findReviewByProductIdAndIdReviewer,
  findReviewsByUserId,
  findReviewsByIdReviewer,
  getAverageRating,
  updateReview,
};
