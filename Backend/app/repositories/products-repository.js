"use strict";

const database = require("../infrastructure/database");

async function addProduct(idSeller, product) {
  const pool = await database.getPool();
  const now = new Date();
  const consulta = `INSERT INTO products(
  idSeller,
  productName,
  price,
  city,
  brand,
  yearOfLaunch,
  yearOfPurchase,
  category,
  actualStatus,
  originalDocuments,
  originalPackage,
  accesories,
  descriptionProduct,
  createdAt
  ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;

  const [created] = await pool.query(consulta, [
    idSeller,
    ...Object.values(product),
    now,
  ]);

  return created.insertId;
}

async function findAllProducts() {
  const pool = await database.getPool();
  const consulta = "SELECT * FROM products ORDER BY createdAt DESC";
  const [products] = await pool.query(consulta);
  return products;
}
async function findProductById(idProduct) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products WHERE id = ? AND deletedAt IS NULL`;
  const [product] = await pool.query(consulta, idProduct);

  return product[0];
}

async function findSelledProductById(idProduct) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products WHERE id = ? AND deletedAt IS NOT NULL`;
  const [product] = await pool.query(consulta, idProduct);

  return product[0];
}

async function findProductsByCreatedAt(createdAtProduct) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products WHERE deletedAt IS NULL ORDER BY createdAt DESC`;
  const [products] = await pool.query(consulta, createdAtProduct);

  return products;
}
async function findProductsByPrice(priceProducts) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products WHERE price <= ?`;
  const [products] = await pool.query(consulta, priceProducts);
  return products;
}
async function findProductAndReviewByIdReviewerAndIdProduct(
  idReviewer,
  idProduct
) {
  const pool = await database.getPool();
  const consulta = `SELECT products.*,reviews.idUser,reviews.content,reviews.rating FROM products INNER JOIN reviews ON products.id = reviews.idProduct WHERE reviews.idReviewer = ? AND products.id = ? AND products.deletedAt IS NOT NULL`;
  const [product] = await pool.query(consulta, [idReviewer, idProduct]);
  return product[0];
}

async function updateProduct(id, product) {
  const {
    productName,
    price,
    city,
    brand,
    yearOfLaunch,
    yearOfPurchase,
    category,
    actualStatus,
    originalDocuments,
    originalPackage,
    accesories,
    descriptionProduct,
  } = product;
  const now = new Date();
  const pool = await database.getPool();
  const updateQuery = `
    UPDATE products
    SET 
    productName = ?,
    price = ?,
    city = ?,
    brand = ?,
    yearOfLaunch = ?,
    yearOfPurchase = ?,
    category = ?,
    actualStatus = ?,
    originalDocuments = ?,
    originalPackage = ?,
    accesories = ?,
    descriptionProduct = ?,
    updatedAt = ?
    WHERE id = ?`;
  await pool.query(updateQuery, [
    productName,
    price,
    city,
    brand,
    yearOfLaunch,
    yearOfPurchase,
    category,
    actualStatus,
    originalDocuments,
    originalPackage,
    accesories,
    descriptionProduct,
    now,
    id,
  ]);
  return true;
}

async function findProductsByCategory(category) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products WHERE category = ? AND deletedAt IS NULL`;
  const [products] = await pool.query(consulta, category);

  return products;
}
async function findProductsByCategoryAndCreatedAt(category) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products WHERE category=? AND deletedAt IS NULL ORDER BY createdAt DESC`;
  const [products] = await pool.query(consulta, category);

  return products;
}
async function removeProductById(idProduct) {
  const pool = await database.getPool();
  const consulta = "DELETE FROM products WHERE id = ?";
  await pool.query(consulta, idProduct);

  return true;
}

async function logicalRemoveProductById(idProduct) {
  const pool = await database.getPool();
  const now = new Date();
  const updateQuery = `
    UPDATE products
    SET deletedAT = ? WHERE id = ?`;
  await pool.query(updateQuery, [now, idProduct]);
  return true;
}

async function findProductsByIdSeller(idSeller) {
  const pool = await database.getPool();
  const consulta =
    "SELECT * FROM products WHERE idSeller = ? AND deletedAt IS NULL";
  const [products] = await pool.query(consulta, idSeller);

  return products;
}

async function findBoughtProductsByIdUser(idUser) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products INNER JOIN transfers ON products.id = transfers.idProduct WHERE idUser = ? ORDER BY moment DESC`;
  const [products] = await pool.query(consulta, idUser);

  return products;
}

async function findSelledProductsByIdSeller(idSeller) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products WHERE idSeller = ? AND deletedAt IS NOT NULL ORDER BY deletedAt DESC`;
  const [products] = await pool.query(consulta, idSeller);

  return products;
}

async function findVisitedProductsByIdUser(idUser) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products INNER JOIN interactions ON products.id = interactions.idProduct WHERE idUser = ? ORDER BY moment DESC`;
  const [products] = await pool.query(consulta, idUser);

  return products;
}

async function findInterestedCategoryProductsByIdUser(idUser) {
  const pool = await database.getPool();
  const consulta = `SELECT category, COUNT(category) AS total FROM products INNER JOIN interactions ON products.id = interactions.idProduct WHERE idUser = ? GROUP BY category ORDER BY total DESC`;
  const [products] = await pool.query(consulta, idUser);

  return products;
}

async function findFavoriteProductsByIdUser(idUser) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products INNER JOIN interactions ON products.id = interactions.idProduct WHERE idUser = ? AND favorite = 'YES' ORDER BY moment DESC`;
  const [products] = await pool.query(consulta, idUser);
  return products;
}

async function findPopularProducts() {
  const pool = await database.getPool();
  const consulta = `SELECT id,idSeller,productName,price,brand,yearOfLaunch,yearOfPurchase,category,actualStatus,originalDocuments,originalPackage,accesories,descriptionProduct, COUNT(idProduct) AS total FROM products INNER JOIN interactions ON products.id = interactions.idProduct GROUP BY id ORDER BY total DESC`;
  const [products] = await pool.query(consulta);
  return products;
}

async function findPopularProductsByCategory(category) {
  const pool = await database.getPool();
  const consulta = `SELECT id,idSeller,productName,price,brand,yearOfLaunch,yearOfPurchase,category,actualStatus,originalDocuments,originalPackage,accesories,descriptionProduct, COUNT(idProduct) AS total FROM products INNER JOIN interactions ON products.id = interactions.idProduct WHERE category = ? GROUP BY id ORDER BY total DESC`;
  const [products] = await pool.query(consulta, category);
  return products;
}

async function findProductsByCategoryAndPrice(category, priceProducts) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM products WHERE category = ? AND price <= ?`;
  const [products] = await pool.query(consulta, [category, priceProducts]);
  console.log(products);
  return products;
}

async function filterProducts(productName, price, city, category) {
  const pool = await database.getPool();
  const consulta =
    "SELECT * FROM products WHERE (? IS NULL OR productName = ?) AND (? IS NULL OR price <= ?) AND (? IS NULL OR city = ?) AND (? IS NULL OR category = ?)";
  const [products] = await pool.query(consulta, [
    productName,
    productName,
    price,
    price,
    city,
    city,
    category,
    category,
  ]);

  return products;
}

module.exports = {
  addProduct,
  findAllProducts,
  findProductById,
  findSelledProductById,
  findProductsByCreatedAt,
  findProductsByPrice,
  updateProduct,
  findProductsByCategory,
  findProductsByCategoryAndCreatedAt,
  removeProductById,
  logicalRemoveProductById,
  findProductsByIdSeller,
  findBoughtProductsByIdUser,
  findSelledProductsByIdSeller,
  findVisitedProductsByIdUser,
  findInterestedCategoryProductsByIdUser,
  findFavoriteProductsByIdUser,
  findPopularProducts,
  findPopularProductsByCategory,
  findProductsByCategoryAndPrice,
  findProductAndReviewByIdReviewerAndIdProduct,
  filterProducts,
};
