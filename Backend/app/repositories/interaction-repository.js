"use strict";

const database = require("../infrastructure/database");

async function getFavorite(idUser, idProduct) {
  const pool = await database.getPool();
  const consulta = `SELECT idProduct FROM interactions WHERE idUser = ? AND idProduct = ? AND favorite = ?`;
  const favorite = await pool.query(consulta, [idUser, idProduct, "YES"]);
  return favorite;
}

async function addInteraction(user) {
  const pool = await database.getPool();
  const now = new Date();
  const consulta = `
    INSERT INTO interactions(
    idUser, idProduct, moment, favorite
    ) VALUES (?, ?, ?, ?)
  `;
  const { idUser, idProduct, favorite } = user;
  const [created] = await pool.query(consulta, [
    idUser,
    idProduct,
    now,
    favorite,
  ]);
  return created;
}

async function removeInteractions(idUser, idProduct) {
  const pool = await database.getPool();
  const consulta = `DELETE FROM interactions
    WHERE idUser = ? AND idProduct = ?`;
  await pool.query(consulta, [idUser, idProduct]);

  return true;
}

module.exports = { addInteraction, removeInteractions, getFavorite };
