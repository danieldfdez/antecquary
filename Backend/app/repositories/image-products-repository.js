"use strict";

const database = require("../infrastructure/database");

async function uploadProductImage(id, image) {
  const pool = await database.getPool();
  const updateQuery =
    "INSERT INTO productImages (idProduct,image) VALUES (?,?)";
  await pool.query(updateQuery, [id, image]);

  return true;
}

async function findImagesProductById(idProduct) {
  const pool = await database.getPool();
  const updateQuery = "SELECT * FROM productImages WHERE idProduct = ?";
  const [imagesProduct] = await pool.query(updateQuery, idProduct);

  return imagesProduct;
}

module.exports = { uploadProductImage, findImagesProductById };
