"use strict";

const database = require("../infrastructure/database");

async function addMessage(idUser, idChat, content) {
  const pool = await database.getPool();
  const now = new Date();
  const insertQuery = `INSERT
    INTO messages (idChat,idUser,content,moment)
    VALUES (?,?,?,?)`;
  await pool.query(insertQuery, [idChat, idUser, content, now]);

  return { idChat, idUser, content, moment: now };
}

async function findMessagesByIdChat(idChat) {
  const pool = await database.getPool();
  const query = "SELECT * FROM messages WHERE idChat = ? ORDER BY moment ASC";
  const [messages] = await pool.query(query, idChat);
  return messages;
}

module.exports = { addMessage, findMessagesByIdChat };
