"use strict";

const express = require("express");
const router = express.Router();
const { createChat } = require("../controllers/chats/create-chat");
const {
  getChatsByUserId,
} = require("../controllers/chats/get-chats-by-user-id");
const {
  logicalDeleteChatByIdProduct,
} = require("../controllers/chats/logical-delete-chat-by-id-product");
const validateAuth = require("../middlewares/validate-auth");

router.route("/").all(validateAuth).get(getChatsByUserId);
router
  .route("/idProduct/:idProduct")
  .all(validateAuth)
  .post(createChat)
  .put(logicalDeleteChatByIdProduct);

module.exports = router;
