"use strict";

const express = require("express");
const router = express.Router();
const {
  createInteraction,
} = require("../controllers/interaction/create-interation");
const {
  deleteInteractionsByIdProduct,
} = require("../controllers/interaction/delete-interactions-by-id-product");
const {
  deleteFavoriteByIdProduct,
} = require("../controllers/interaction/delete-favorite-by-id-product");
const validateAuth = require("../middlewares/validate-auth");

router
  .route("/")
  .all(validateAuth)
  .post(createInteraction)
  .delete(deleteInteractionsByIdProduct);
router.route("/favorite").all(validateAuth).delete(deleteFavoriteByIdProduct);

module.exports = router;
