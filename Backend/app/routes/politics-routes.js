"use strict";

const express = require("express");
const router = express.Router();
const { createPolitic } = require("../controllers/politics/create-politic");
const {
  updatePoliticByDate,
} = require("../controllers/politics/update-by-date");
const {
  deletePoliticByDate,
} = require("../controllers/politics/delete-by-date");
const { getPolitics } = require("../controllers/politics/get-politics");
const validateAuth = require("../middlewares/validate-auth");

router.route("/").get(getPolitics);
router
  .route("/")
  .all(validateAuth)
  .post(createPolitic)
  .put(updatePoliticByDate)
  .delete(deletePoliticByDate);

module.exports = router;
