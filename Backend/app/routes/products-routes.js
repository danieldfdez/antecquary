"use strict";

const express = require("express");
const router = express.Router();

const { createProduct } = require("../controllers/products/create-products");
const { deleteProductById } = require("../controllers/products/delete-product");
const { getProducts } = require("../controllers/products/get-products");
const { getProductById } = require("../controllers/products/get-product-by-id");
const {
  getProductsByCreatedAt,
} = require("../controllers/products/get-products-by-created-at");
const {
  getProductsByPrice,
} = require("../controllers/products/get-products-by-price");
const {
  updateProductById,
} = require("../controllers/products/update-product-by-id");
const {
  getProductsByCategory,
} = require("../controllers/products/get-products-by-category");
const {
  getProductsByCategoryAndCreatedAt,
} = require("../controllers/products/get-products-by-category-and-created-at");
const {
  getProductsByIdSeller,
} = require("../controllers/products/get-products-by-id-seller");
const {
  getBoughtProductsByIdUser,
} = require("../controllers/products/get-bought-products-by-id-user");
const {
  getSelledProductsByIdSeller,
} = require("../controllers/products/get-selled-products-by-id-seller");
const {
  getVisitedProductsByIdUser,
} = require("../controllers/products/get-visited-products-by-id-user");
const {
  getInterestedCategoryProductsByIdUser,
} = require("../controllers/products/get-interested-category-products-by-id-user");
const {
  getFavoriteProductsByIdUser,
} = require("../controllers/products/get-favorite-products-by-id-user");
const {
  getPopularProducts,
} = require("../controllers/products/get-popular-products");
const {
  getPopularProductsByCategory,
} = require("../controllers/products/get-popular-products-by-category");
const {
  getProductsByCategoryAndPrice,
} = require("../controllers/products/get-products-by-category-and-price");
const {
  getProductAndReviewByIdReviewerAndIdProduct,
} = require("../controllers/products/get-product-and-review-by-id-reviewer-and-id-product");
const {
  getFilterProducts,
} = require("../controllers/products/get-filter-products");
const validateAuth = require("../middlewares/validate-auth");

// Endpoint Públicos
router.route("/id/:idProduct").get(getProductById);
router.route("/sortBy/recent").get(getProductsByCreatedAt);
router.route("/sortBy/popular").get(getPopularProducts);
router.route("/sortBy/:price").get(getProductsByPrice);
router.route("/category/:category").get(getProductsByCategory);
router
  .route("/category/:category/sortBy/recent")
  .get(getProductsByCategoryAndCreatedAt);
router
  .route("/category/:category/sortBy/popular")
  .get(getPopularProductsByCategory);
router
  .route("/category/:category/sortBy/:price")
  .get(getProductsByCategoryAndPrice);
router.route("/filter").get(getFilterProducts);
router.route("/idSeller/:idSeller").get(getProductsByIdSeller);

// Endpoint Privados
router.route("/").all(validateAuth).get(getProducts);
router.route("/").all(validateAuth).post(createProduct);
router.route("/id/:idProduct").all(validateAuth).put(updateProductById);
router.route("/id/:idProduct").all(validateAuth).delete(deleteProductById);
router
  .route("/idProduct/:idProduct")
  .all(validateAuth)
  .get(getProductAndReviewByIdReviewerAndIdProduct);
router.route("/bought").all(validateAuth).get(getBoughtProductsByIdUser);
router.route("/selled").all(validateAuth).get(getSelledProductsByIdSeller);
router.route("/visited").all(validateAuth).get(getVisitedProductsByIdUser);
router
  .route("/interestedCategorys")
  .all(validateAuth)
  .get(getInterestedCategoryProductsByIdUser);
router.route("/favorite").all(validateAuth).get(getFavoriteProductsByIdUser);

module.exports = router;
