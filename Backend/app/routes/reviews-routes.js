"use strict";
const express = require("express");
const router = express.Router();
const {
  createReview,
} = require("../controllers/reviews/create-review-by-product-id");
const { getReviews } = require("../controllers/reviews/get-reviews");
const {
  deleteReview,
} = require("../controllers/reviews/delete-review-by-id-review");
const {
  getReviewsByUserId,
} = require("../controllers/reviews/get-reviews-by-id-user");
const {
  getReviewByProductIdAndIdReviewer,
} = require("../controllers/reviews/get-review-by-id-product-and-id-reviewer");
const {
  getReviewsByIdReviewer,
} = require("../controllers/reviews/get-reviews-by-id-reviewer");
const {
  getValorationByUserId,
} = require("../controllers/reviews/get-valoration-by-id-user");
const {
  updateReviewByIdReview,
} = require("../controllers/reviews/update-review-by-id-review");
const validateAuth = require("../middlewares/validate-auth");

router.route("/idUser/:idUser").get(getReviewsByUserId);
router.route("/valoration/:idUser").get(getValorationByUserId);

router.route("/").all(validateAuth).get(getReviews);
router.route("/idProduct/:idProduct").all(validateAuth).post(createReview);
router
  .route("/idProduct/:idProduct")
  .all(validateAuth)
  .get(getReviewByProductIdAndIdReviewer);
router
  .route("/idReview/:idReview")
  .all(validateAuth)
  .put(updateReviewByIdReview)
  .delete(deleteReview);
router
  .route("/idReviewer/:idReviewer")
  .all(validateAuth)
  .get(getReviewsByIdReviewer);

module.exports = router;
