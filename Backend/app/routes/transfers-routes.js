"use strict";

const express = require("express");
const router = express.Router();
const { createTransfer } = require("../controllers/transfers/create-transfer");
const {
  deleteTransferByProductId,
} = require("../controllers/transfers/delete-transfer-by-product-id");
const {
  getTransfersByUserId,
} = require("../controllers/transfers/get-transfers-by-user-id");
const {
  getTransferByIdProduct,
} = require("../controllers/transfers/get-transfer-by-id-product");
const validateAuth = require("../middlewares/validate-auth");

router
  .route("/")
  .all(validateAuth)
  .delete(deleteTransferByProductId)
  .get(getTransfersByUserId);
router.route("/idProduct/:idProduct").get(getTransferByIdProduct);
router
  .route("/idUser/:idUser/idProduct/:idProduct")
  .all(validateAuth)
  .post(createTransfer);

module.exports = router;
