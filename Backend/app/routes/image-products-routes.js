"use strict";

const express = require("express");
const router = express.Router();
const {
  uploadImageProduct,
} = require("../controllers/image-products/upload-image-product");
const {
  getImagesProductById,
} = require("../controllers/image-products/get-images-product");
const validateAuth = require("../middlewares/validate-auth");

router.route("/:idProduct").get(getImagesProductById);

router.route("/:idProduct").all(validateAuth).post(uploadImageProduct);

module.exports = router;
