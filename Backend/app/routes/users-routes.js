"use strict";

const express = require("express");
const router = express.Router();
const { registerUser } = require("../controllers/users/register-user");
const { activateUser } = require("../controllers/users/activation-account");
const { loginUser } = require("../controllers/users/login-user");
const { getUserProfile } = require("../controllers/users/get-user-profile");
const {
  findUserByNickname,
} = require("../controllers/users/get-user-by-nickname");
const { getUserById } = require("../controllers/users/get-user-by-id");
const { updateUser } = require("../controllers/users/update-user");
const { deleteUserById } = require("../controllers/users/delete-user-by-id");
const {
  getUserReviewsById,
} = require("../controllers/users/get-users-reviews-by-id");
const { getUsers } = require("../controllers/users/get-users");
const {
  uploadImageProfile,
} = require("../controllers/users/upload-image-profile");
const validateAuth = require("../middlewares/validate-auth");

router.route("/register").post(registerUser);
router.route("/activation").get(activateUser);
router.route("/login").post(loginUser);
router.route("/idUser/:idUser").get(getUserById);
router.route("/nickname/:nickname").get(findUserByNickname);

router.route("/profile").all(validateAuth).get(getUserProfile);
router.route("/").all(validateAuth).put(updateUser).get(getUsers);
router.route("/:id").all(validateAuth).delete(deleteUserById);
router.route("/:id/reviews").all(validateAuth).get(getUserReviewsById);
router.route("/upload").all(validateAuth).post(uploadImageProfile);

module.exports = router;
