"use strict";

const express = require("express");
const router = express.Router();
const { createMessage } = require("../controllers/messages/create-message");
const {
  getMessagesByIdChat,
} = require("../controllers/messages/get-messages-by-id-chat");
const validateAuth = require("../middlewares/validate-auth");

router
  .route("/idChat/:idChat")
  .all(validateAuth)
  .post(createMessage)
  .get(getMessagesByIdChat);

module.exports = router;
