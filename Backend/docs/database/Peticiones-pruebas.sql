USE AnTecQuary;
SELECT * FROM users;
SELECT * FROM interactions;
SELECT * FROM transfers;
SELECT * FROM politics;
SELECT * FROM products;
SELECT * FROM reviews;
SELECT * FROM productImages;
UPDATE users SET rol='admin' WHERE id=1;
UPDATE users SET rol='reader' WHERE id=1;
SELECT idProduct FROM interactions WHERE idUser = 1 AND idProduct = 1 AND favorite = YES;
SELECT * FROM products INNER JOIN transfers ON products.id = transfers.idProduct WHERE idUser = 1 ORDER BY moment DESC; 
-- petición para todos los articulos de un usuario que ha comprado ordenadas en el tiempo
SELECT * FROM products WHERE idSeller = 1 AND deletedAt IS NOT NULL ORDER BY deletedAt DESC;
-- petición para obtener todos los articulos vendidos por el usuario
SELECT * FROM products INNER JOIN interactions ON products.id = interactions.idProduct WHERE idUser = 1 ORDER BY moment DESC;
-- peticion para todos los articulos que se han visitado anteriormente por un usuario ordenadas en el tiempo
SELECT category FROM products INNER JOIN interactions ON products.id = interactions.idProduct WHERE idUser = 1 ORDER BY moment DESC;
-- peticion para obtener las categorias de objetos recientemente visitadas por el usuario ordenadas en el tiempo
SELECT * FROM products INNER JOIN interactions ON products.id = interactions.idProduct WHERE idUser = 1 AND favorite = 'YES' ORDER BY moment DESC;
-- peticion para obtener todos los artículos que el usuario ha marcado como favoritos
SELECT * FROM products WHERE idSeller = 1 AND deletedAt IS NULL;
