DROP DATABASE IF EXISTS AnTecQuary;
CREATE DATABASE IF NOT EXISTS AnTecQuary;
USE AnTecQuary;

CREATE TABLE IF NOT EXISTS users (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    firstName varchar(255) NOT NULL,
    lastName varchar(255) NOT NULL,
    birthday varchar(20) NOT NULL,
    sex ENUM('hombre','mujer','no especificar') DEFAULT 'no especificar',
    NIF varchar(10) NOT NULL,
    country varchar(255) NOT NULL,
    city varchar(255) NOT NULL,
    adress varchar(255) NOT NULL,
    CP INT NOT NULL,
    phone INT NULL,
    nickname varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    rol ENUM('admin', 'reader') DEFAULT 'reader',
    image VARCHAR(255) NULL,
    createdAt DATETIME NOT NULL,
    updatedAt DATETIME NULL,
	verificationCode VARCHAR(100) NULL,
	verifiedAt DATETIME NULL
);

CREATE TABLE IF NOT EXISTS products (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idSeller INT NOT NULL,
    productName varchar(255) NOT NULL,
    price INT NOT NULL,
    city varchar(255) NOT NULL,
    brand varchar(255) NOT NULL,
    yearOfLaunch INT NOT NULL,
    yearOfPurchase INT NOT NULL,
    category ENUM('videojuegos','ordenadores','camaras','telefonia','audiovisual','otros') DEFAULT 'otros',
    actualStatus ENUM('malo','defectuoso','muy usado','desgastado','funcional','bueno','perfecto','sin uso') DEFAULT 'funcional',
    originalDocuments ENUM ('no tiene','en mal estado','en buen estado','faltan partes','completa','impecable') DEFAULT 'no tiene',
    originalPackage ENUM ('no tiene','en mal estado','en buen estado','impecable') DEFAULT 'no tiene',
    accesories varchar(255) NULL,
    descriptionProduct varchar(1000) NOT NULL,
    createdAt DATETIME NOT NULL,
    updatedAt DATETIME NULL,
    deletedAt DATETIME NULL,
    FOREIGN KEY (`idSeller`) REFERENCES `users` (`id`) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS productImages (
    idProduct INT NOT NULL,
    image VARCHAR(255) NULL,
    FOREIGN KEY (`idProduct`) REFERENCES `products` (`id`) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS reviews (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idUser int NOT NULL,
	idProduct int NOT NULL,
    idReviewer int NOT NULL,
	content varchar(1000) NOT NULL,
	rating int NOT NULL,
	createdAt DATETIME NOT NULL,
	updatedAt DATETIME NULL,
    deletedAt DATETIME NULL,
	FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`idReviewer`) REFERENCES `users` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`idProduct`) REFERENCES `products` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS interactions (
	idUser INT NOT NULL,
    idProduct INT NOT NULL,
    moment DATETIME NOT NULL,
    favorite ENUM('YES','NOT') DEFAULT 'NOT',
    FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`idProduct`) REFERENCES `products` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS transfers (
	idUser INT NOT NULL,
    idProduct INT NOT NULL UNIQUE,
    moment DATETIME NULL,
    FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`idProduct`) REFERENCES `products` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS chats (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idClient INT NOT NULL,
    idProduct INT NOT NULL,
    deletedAt DATETIME NULL,
    FOREIGN KEY (`idClient`) REFERENCES `users` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`idProduct`) REFERENCES `products` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS messages (
	idChat INT NOT NULL,
    idUser INT NOT NULL,
    content varchar(500) NOT NULL,
	moment DATETIME NOT NULL,
    FOREIGN KEY (`idChat`) REFERENCES `chats` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE
);
 CREATE TABLE IF NOT EXISTS politics (
	expeditionDate datetime NOT NULL,
    content varchar(10000) NOT NULL
 );

 -- CREATE TABLE IF NOT EXISTS helpers (
--      question VARCHAR(1000) NOT NULL,
--      answer VARCHAR(1000) NOT NULL
--  );