# Products-ROUTES # Products-CONTROLLERS

GET /api/v1/products -> getProducts -> Admin -> Listado de los productos
GET /api/v1/products/:id -> getProductById -> Publica -> Información de un producto
GET /api/v1/products/recientes -> getProductsByCreatedAt -> Publica -> Listado de productos añadidos en los últimos días a la plataforma
GET /api/v1/products/populares -> getProductsByInteractions -> Publica -> Listado de productos con más interacciones
GET /api/v1/products/baratos -> getProductsByPrice -> Publica -> Listado de productos por debajo de cierto precio
GET /api/v1/products/:type -> getProductsByType
-> Publica -> Listado de los productos por tipo
GET /api/v1/products/:type/recientes -> getProductsByTypeByCreatedAt
-> Publica -> Listado de los productos por tipo añadidos en los últimos días a la plataforma
GET /api/v1/products/:type/populares -> getProductsByTypeByInteractions
-> Publica -> Listado de los productos por tipo con más interacciones
GET /api/v1/products/:type/baratos -> getProductsByTypeByPrice
-> Publica -> Listado de los productos por tipo por debajo de cierto precio
GET /api/v1/products/:idUser -> getProductsByIdUser -> Usuario -> Listado de productos a la venta del usuario
GET /api/v1/products/:idUser/interactions -> getProductsByIdUserInteractions -> Usuario -> Listado de productos visitados en fechas recientes por el usuario
GET /api/v1/products/:idUser/interactions/tipe -> getProductsByIDUserInteractionsTipe
-> Usuario -> Listado de productos relacionados por tipo con los productos anteriormente visitados por el usuario
GET /api/v1/products/:idUser/interactions/favorite -> getProductsByIdUserInteractionsFavorite
-> Usuario -> Listado de productos marcados por el usuario como favoritos

POST /api/v1/products -> createProduct -> Usuario -> Crear un producto para su posterior venta
PUT /api/v1/products/:id -> updateProductById -> Admin/Usuario -> Actualizar un producto. Lo puede hacer tambien el admin, enviara un correo de información sobre los motivos del cambio.
DELETE /api/v1/products/:id -> deleteProductById -> Admin/Usuario -> Eliminar un producto por ID. En caso de hacerlo el admin, enviará un correo con información sobre el motivo del borrado.

# REVIEWS-ROUTES # REVIEWS-CONTROLLERS

GET /api/v1/reviews -> getReviews -> Admin -> Listado de todas las reviews
GET /api/v1/reviews/:idUser -> getReviewsByIdUser
-> Publica -> Listado de las reviews de un usuario
POST /api/v1/reviews -> createReview -> Usuario -> Crear una review de un usuario.
DELETE /api/v1/reviews/:id -> deleteReview -> Usuario/Admin -> Eliminar una review por ID. En caso de hacerlo el admin, enviará un correo con información sobre el motivo del borrado.

# USERS-ROUTES # USERS-CONTROLLERS

POST /api/v1/users/register -> registerUser -> Publica -> Registrar un usuario nuevo
POST /api/v1/users/login -> loginUser -> Publica -> Loguearse en la aplicacion
GET /api/v1/users/activation -> activateUser -> Publica -> Link para activar las cuentas
PUT /api/v1/users -> updateUser -> Usuario/Admin -> La ejecuta el propio usuario (obtiene el ID del accessToken). Lo puede hacer tambien el admin, enviara un correo de información sobre los motivos del cambio.
POST /api/v1/user/upload -> uploadImageProfile -> Usuario -> El propio usuario puede cambiar el avatar

GET /api/v1/users/profile -> getUserProfile -> Usuario/Admin -> Perfil del usuario.
GET /api/v1/users -> getUsers -> Admin -> Listado usuarios
GET /api/v1/users/:id/reviews -> getUserReviewsById -> Admin -> Ver reviews hechas por un usuario
DELETE /api/v1/users/:id -> deleteUserById -> Usuario/Admin -> Eliminar un usuario. En caso de hacerlo el admin, enviará un correo con información sobre el motivo del borrado.
