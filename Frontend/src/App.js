import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { TokenProvider } from "./Components/TokenContext";
import { Navbar } from "./Components/Navbar";
import { Principal } from "./Paginas app/Principal";
import { SeccionAudiovisual } from "./Paginas app/SeccionAudiovisual";
import { SeccionCamaras } from "./Paginas app/SeccionCamaras";
import { SeccionOrdenadores } from "./Paginas app/SeccionOrdenadores";
import { SeccionOtros } from "./Paginas app/SeccionOtros";
import { SeccionTelefonia } from "./Paginas app/SeccionTelefonia";
import { SeccionVideojuegos } from "./Paginas app/SeccionVideojuegos";
import { Registro } from "./Paginas app/Registro";
import { Acceso } from "./Paginas app/Acceso";
import { UsuarioPublico } from "./Paginas app/UsuarioPublico";
import { Perfil } from "./Paginas app/Perfil";
import { CreacionProducto } from "./Paginas app/CreacionProducto";
import { Producto } from "./Paginas app/Producto";
import { ModificacionProducto } from "./Paginas app/ModificacionProducto";
import { ChatProducto } from "./Paginas app/ChatProducto";
import { CrearReview } from "./Paginas app/CrearReview";
import { UserProfilePersonal } from "./Components/UserProfilePersonal";
import { ProductsUser } from "./Components/ProductsUser";
import { ChatsUser } from "./Components/ChatsUser";
import { HistoryUser } from "./Components/HistoryUser";

function App() {
  return (
    <div className="App">
      <Router basename="/antecquary">
        <TokenProvider>
          <Navbar></Navbar>
          <Switch>
            <Route exact path="/">
              <Principal></Principal>
            </Route>
            <Route exact path="/audiovisual">
              <SeccionAudiovisual></SeccionAudiovisual>
            </Route>
            <Route exact path="/camaras">
              <SeccionCamaras></SeccionCamaras>
            </Route>
            <Route exact path="/ordenadores">
              <SeccionOrdenadores></SeccionOrdenadores>
            </Route>
            <Route exact path="/otros">
              <SeccionOtros></SeccionOtros>
            </Route>
            <Route exact path="/telefonia">
              <SeccionTelefonia></SeccionTelefonia>
            </Route>
            <Route exact path="/videojuegos">
              <SeccionVideojuegos></SeccionVideojuegos>
            </Route>
            <Route exact path="/register">
              <Registro></Registro>
            </Route>
            <Route exact path="/login">
              <Acceso></Acceso>
            </Route>
            <Route exact path="/user/:idUser">
              <UsuarioPublico></UsuarioPublico>
            </Route>
            <Route exact path="/profile">
              <Perfil></Perfil>
            </Route>
            <Route exact path="/new-product">
              <CreacionProducto></CreacionProducto>
            </Route>
            <Route exact path="/product/:idProduct">
              <Producto></Producto>
            </Route>
            <Route exact path="/product/update/:idProduct">
              <ModificacionProducto></ModificacionProducto>
            </Route>
            <Route exact path="/chat/:idChat/:idProduct/:idUser">
              <ChatProducto></ChatProducto>
            </Route>
            <Route exact path="/new-review/:idProduct/:idUser">
              <CrearReview></CrearReview>
            </Route>
            <Route exact path="/personalData">
              <UserProfilePersonal></UserProfilePersonal>
            </Route>
            <Route exact path="/myProducts/:idUser">
              <ProductsUser></ProductsUser>
            </Route>
            <Route exact path="/myChats">
              <ChatsUser></ChatsUser>
            </Route>
            <Route exact path="/history">
              <HistoryUser></HistoryUser>
            </Route>
          </Switch>
        </TokenProvider>
      </Router>
    </div>
  );
}

export { App };
