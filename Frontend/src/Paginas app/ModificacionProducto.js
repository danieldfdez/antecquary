import { ProductUpdate } from "../Components/ProductUpdate";
import { useParams } from "react-router-dom";

export const ModificacionProducto = () => {
  const { idProduct } = useParams();
  return <ProductUpdate idProduct={idProduct}></ProductUpdate>;
};
