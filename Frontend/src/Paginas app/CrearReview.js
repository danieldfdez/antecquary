import { CreateReview } from "../Components/CreateReview";
import { useParams } from "react-router-dom";

export const CrearReview = () => {
  const { idProduct, idUser } = useParams();
  return <CreateReview idProduct={idProduct} idUser={idUser}></CreateReview>;
};
