import { Product } from "../Components/Product";
import { useParams } from "react-router-dom";

export const Producto = () => {
  const { idProduct } = useParams();
  return <Product idProduct={idProduct}></Product>;
};
