import { PublicUser } from "../Components/PublicUser";
import { useParams } from "react-router-dom";

export const UsuarioPublico = () => {
  const { idUser } = useParams();
  return <PublicUser idUser={idUser}></PublicUser>;
};
