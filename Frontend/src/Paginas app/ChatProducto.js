import { Chat } from "../Components/Chat";
import { useParams } from "react-router-dom";

export const ChatProducto = () => {
  const { idChat, idProduct, idUser } = useParams();
  return <Chat idChat={idChat} idProduct={idProduct} idUser={idUser}></Chat>;
};
