import { Avatar } from "./Avatar";
import { MessageBody } from "./MessageBody";
import { useUserPublic } from "../hooks/remoteHooks";

const Message = ({ msg }) => {
  const [userData] = useUserPublic(msg.idUser);
  return (
    <div style={{ display: "flex", padding: "10px" }}>
      <Avatar img={userData.image} />
      <MessageBody userName={userData.nickname} content={msg.content} />
      {msg.moment}
    </div>
  );
};

export { Message };
