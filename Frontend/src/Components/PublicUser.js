import { useEffect, useState } from "react";
import { useUserPublic, useUserProducts } from "../hooks/remoteHooks";
import { Avatar } from "./Avatar";
import { ValorationUser } from "./ValorationUser";
import { Review } from "./Review";
import { Carrousel } from "./Carrousel";

export const PublicUser = ({ idUser }) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [userData] = useUserPublic(idUser);
  const [userProducts] = useUserProducts(idUser);
  const [reviews, setReviews] = useState([]);
  const [count, setCount] = useState(0);
  const indexMap = count * 6;

  useEffect(() => {
    const getReviews = async () => {
      const res = await (
        await fetch(`http://localhost:3000/api/v1/reviews/idUser/${idUser}`)
      ).json();
      if (res.error) {
        setErrorMsg(res.error);
      } else {
        setReviews(res);
        setErrorMsg(null);
      }
    };
    getReviews();
  }, [idUser]);

  if (!userData) return <></>;
  if (!userProducts) return <></>;
  if (!reviews) return <></>;

  const next = (e) => {
    e.preventDefault();
    if (indexMap >= reviews.length - 6) {
      setCount(0);
    } else {
      setCount(count + 1);
    }
  };

  const previous = (e) => {
    e.preventDefault();
    if (indexMap === 0) {
      return true;
    } else {
      setCount(count - 1);
    }
  };

  const reviewsList = reviews
    .filter((review, index) => index >= indexMap && index < indexMap + 6)
    .map((review) => (
      <Review reviewData={review} idUser={review.idReviewer}></Review>
    ));
  return (
    <div className="center">
      <div className="section-background">
        <div className="personal-head">
          <Avatar img={userData.image}></Avatar>
          <ValorationUser idUser={idUser}></ValorationUser>
        </div>
        <div>
          {errorMsg ? (
            <div style={{ color: "red" }}>{errorMsg}</div>
          ) : (
            <div>{reviewsList}</div>
          )}
          <form>
            <button className="button-all-page" onClick={previous}>
              Anterior
            </button>
            <button className="button-all-page" onClick={next}>
              Siguiente
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};
