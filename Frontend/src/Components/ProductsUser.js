import { useContext, useEffect, useState } from "react";
import { TokenContext } from "./TokenContext";
import { LittleProduct } from "./LittleProduct";
import { useUserProducts } from "../hooks/remoteHooks";
import { Redirect, useParams } from "react-router-dom";

export const ProductsUser = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const { idUser } = useParams();
  const [userProducts] = useUserProducts(idUser);
  const [products, setProducts] = useState([]);
  const [count, setCount] = useState(0);
  const indexMap = count * 4;
  useEffect(() => {
    if (userProducts.error) {
      setErrorMsg(userProducts.error);
    } else {
      setProducts(userProducts);
      setErrorMsg(null);
    }
  }, [userProducts]);

  const next = (e) => {
    e.preventDefault();
    if (indexMap >= products.length - 4) {
      setCount(0);
    } else {
      setCount(count + 1);
    }
  };

  const previous = (e) => {
    e.preventDefault();
    if (indexMap === 0) {
      return true;
    } else {
      setCount(count - 1);
    }
  };
  const listItems = products
    .filter((product, index) => index >= indexMap && index < indexMap + 4)
    .map((product, index) => {
      return (
        <LittleProduct
          key={`idLittleProductUser${index}`}
          dataProduct={product}
        />
      );
    });

  return (
    <section>
      {errorMsg ? (
        <div style={{ color: "red" }}>{errorMsg}</div>
      ) : token ? (
        <>
          {listItems}
          <form>
            <button onClick={previous}>Anterior</button>
            <button onClick={next}>Siguiente</button>
          </form>
        </>
      ) : (
        <Redirect to="/login" />
      )}
    </section>
  );
};
