import { useState, useContext } from "react";
import { TokenContext } from "./TokenContext";
import { Redirect } from "react-router-dom";

export const CreateChat = ({ idProduct, idUser }) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const [link, setLink] = useState();

  const handleCreateChat = async (e) => {
    e.preventDefault();
    const res = await fetch(
      `http://localhost:3000/api/v1/chats/idProduct/${idProduct}`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const bodyDeLaRespuesta = await res.json();
    if (res.ok) {
      setErrorMsg(null);
      setLink(bodyDeLaRespuesta);
    } else {
      setErrorMsg(`${bodyDeLaRespuesta.error}`);
    }
  };
  return (
    <>
      {link ? (
        <Redirect to={`/chat/${link.idChat}/${idProduct}/${idUser}`} />
      ) : token ? (
        <form onSubmit={handleCreateChat}>
          <button className="button-all-page" type="submit">
            Contactar con el vendedor
          </button>
          {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
        </form>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
};
