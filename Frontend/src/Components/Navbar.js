import { useState } from "react";
import { Dropdown } from "./Dropdown";
import { Button } from "./Button";
import { Link } from "react-router-dom";

export function Navbar() {
  const [click, setClick] = useState(false);
  const handlerClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);
  const [dropdown, setDropdown] = useState(false);
  const onMouseEnter = () => {
    if (window.innerWidth < 960) {
      setDropdown(false);
    } else {
      setDropdown(true);
    }
  };
  const onMouseLeave = () => {
    if (window.innerWidth < 960) {
      setDropdown(false);
    } else {
      setDropdown(false);
    }
  };
  return (
    <>
      <nav className="navbar">
        <Link to="/" className="navbar-logo">
          <img className="logo" src="/images/Logo.jpg" alt="AntecquaryLogo" />
          ANTECQUARY
        </Link>
        <div className="menu-icon" onClick={handlerClick}>
          <i className={click ? "fas fa-times" : "fas fa-bars"} />
        </div>
        <ul className={click ? "nav-menu active" : "nav-menu"}>
          <li className="nav-item">
            <Link to="/" className="nav-links" onClick={closeMobileMenu}>
              INICIO
            </Link>
          </li>
          <li
            className="nav-item"
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
          >
            <Link
              to="/products"
              className="nav-links"
              onClick={closeMobileMenu}
            >
              PRODUCTOS <i className="fas fa-caret-down" />
            </Link>
            {dropdown && <Dropdown />}
          </li>
          <li className="nav-item">
            <Link
              to="/register"
              className="nav-links"
              onClick={closeMobileMenu}
            >
              REGISTRATE
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/help" className="nav-links" onClick={closeMobileMenu}>
              AYUDA
            </Link>
          </li>
          <li className="nav-item">
            <Link
              to="/login"
              className="nav-links-mobile"
              onClick={closeMobileMenu}
            >
              INICIA SESION
            </Link>
          </li>
        </ul>
        <Button></Button>
      </nav>
    </>
  );
}
