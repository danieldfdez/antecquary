import { useState, useEffect } from "react";

export const ValorationUser = ({ idUser }) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [valoration, setValoration] = useState([]);

  useEffect(() => {
    const getValoration = async () => {
      const res = await (
        await fetch(`http://localhost:3000/api/v1/reviews/valoration/${idUser}`)
      ).json();
      if (res.error) {
        setErrorMsg(`${res.error}`);
      } else {
        setErrorMsg(null);
        setValoration(res);
      }
    };
    getValoration();
  }, [idUser]);

  if (!idUser) return <></>;
  return (
    <div>
      {errorMsg ? (
        <p>Valoración: {`${errorMsg}`}</p>
      ) : (
        <p>Valoración: {`${valoration}`}</p>
      )}
    </div>
  );
};
