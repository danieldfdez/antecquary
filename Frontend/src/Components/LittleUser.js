import { Avatar } from "./Avatar";
import { ValorationUser } from "./ValorationUser";
import { useUserPublic } from "../hooks/remoteHooks";
import { Link } from "react-router-dom";

export const LittleUser = ({ idUser }) => {
  const [userData] = useUserPublic(idUser);
  if (!userData) return <></>;
  return (
    <div className="little-user-info">
      <Avatar img={`${userData.image}`}></Avatar>
      <section className="modal-product-content">
        <p>
          Usuario: {`${userData.nickname}`}
          <ValorationUser idUser={idUser}></ValorationUser>
        </p>
        <Link className="button-all-page" to={`/user/${idUser}`}>
          Ir a perfil
        </Link>
      </section>
    </div>
  );
};
