import React, { useRef } from "react";
import { Link } from "react-router-dom";
import { ProductImagesLittle } from "./ProductImagesPequeno";

function ModalProducts({ showModal, setShowModal, productData }) {
  const modalRef = useRef();

  const closeModalProduct = (e) => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  return (
    <>
      {showModal ? (
        <div
          className="modal-background"
          ref={modalRef}
          onClick={closeModalProduct}
        >
          <div className="modal-product-wrapper">
            <ProductImagesLittle
              imgClass="modal-product-image"
              idProduct={productData.id}
            ></ProductImagesLittle>
            <div className="modal-product-content">
              <div>Articulo: {productData.productName}</div>
              <div>Precio: {productData.price}</div>
              <div>Ciudad: {productData.city}</div>
              <div>Categoria: {productData.category}</div>
              <Link
                className="button-all-page"
                to={`/product/${productData.id}`}
              >
                Más información
              </Link>
            </div>
            <button className="button-close-modal" onClick={closeModalProduct}>
              X
            </button>
          </div>
        </div>
      ) : null}
    </>
  );
}

export default ModalProducts;
