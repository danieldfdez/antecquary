export const MenuItems = [
  {
    title: "AUDIOVISUAL",
    path: "/audiovisual",
    cName: "dropdown-link",
  },
  {
    title: "CAMARAS",
    path: "/camaras",
    cName: "dropdown-link",
  },

  {
    title: "VIDEOJUEGOS",
    path: "/videojuegos",
    cName: "dropdown-link",
  },
  {
    title: "ORDENADORES",
    path: "/ordenadores",
    cName: "dropdown-link",
  },
  {
    title: "TELEFONIA",
    path: "/telefonia",
    cName: "dropdown-link",
  },
  {
    title: "OTROS",
    path: "/otros",
    cName: "dropdown-link",
  },
];
