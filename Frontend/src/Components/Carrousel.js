import { useState } from "react";
import { ModalContainer } from "./ModalContainer";

export function Carrousel({ productsData }) {
  const [directionClass, setDirectionClass] = useState("");
  const [count, setCount] = useState(0);
  const indexMap = count * 9;

  const next = (e) => {
    e.preventDefault();
    if (indexMap >= products.length - 9) {
      setCount(0);
    } else {
      setCount(count + 1);
    }
  };

  const previous = (e) => {
    e.preventDefault();
    if (indexMap === 0) {
      return true;
    } else {
      setCount(count - 1);
    }
  };
  const handlerClickRight = () => {
    setDirectionClass("carrousel-click-right");
    const interval = setInterval(() => {
      setDirectionClass("");
    }, 500);
    return () => {
      clearInterval(interval);
    };
  };
  const handlerClickLeft = () => {
    setDirectionClass("carrousel-click-left");
    const interval = setInterval(() => {
      setDirectionClass("");
    }, 500);
    return () => {
      clearInterval(interval);
    };
  };

  const productsToShow = productsData.filter(
    (product, index) => index >= indexMap && index < indexMap + 9
  );

  if (productsToShow.length < 9) {
    for (let i = 0; i < 9 - productsToShow.length; i++) {
      productsToShow.push("");
    }
  }

  const products = productsToShow.map((product) => {
    if (!product) {
      return <div></div>;
    }
    return (
      <>
        {/* <div className={click ? "carrousel-click" : "carrousel360"}> */}
        <div>
          <ModalContainer productData={product}></ModalContainer>
        </div>
        {/* </div> */}
      </>
    );
  });
  // carousel__face;
  return (
    <>
      <div className={`carrousel-click ${directionClass}`}>{products}</div>
      <div className="manual-controls">
        <button className="carrousel-prev" onClick={handlerClickLeft}>
          ❮
        </button>
        <button className="carrousel-next" onClick={handlerClickRight}>
          ❯
        </button>
      </div>
      <form>
        <button onClick={previous}>Anterior</button>
        <button onClick={next}>Siguiente</button>
      </form>
    </>
  );
}
