import { useContext, useEffect, useState } from "react";
import { TokenContext } from "./TokenContext";
import { Redirect, Link } from "react-router-dom";
import { ProductImages } from "./ProductImages";
import { Review } from "./Review";
import {
  useTransferByProduct,
  useProfile,
  useReviewByIdProductAndIdReviewer,
} from "../hooks/remoteHooks";

export const LittleProduct = ({ dataProduct, transferType }) => {
  const [token] = useContext(TokenContext);
  const [data] = useProfile(token);
  const [transfer] = useTransferByProduct(dataProduct.id);
  const [user, setUser] = useState();
  const [review] = useReviewByIdProductAndIdReviewer(dataProduct.id, token);
  useEffect(() => {
    if (transfer && data.id === dataProduct.idSeller) {
      setUser(transfer.idUser);
    } else if (transfer && data.id === transfer.idUser) {
      setUser(dataProduct.idSeller);
    }
  }, [transfer, data.id, dataProduct.idSeller]);
  if (!transfer) return <></>;
  if (!data) return <></>;
  if (!review) return <></>;

  return (
    <>
      {dataProduct.deletedAt && !review.error ? (
        <article>
          <ProductImages idProduct={dataProduct.id} />
          <ul>
            <li>Nombre del producto: {dataProduct.productName}</li>
            <li>Precio: {dataProduct.price}</li>
            <li>Categoría: {dataProduct.category}</li>
            <li>Localidad: {dataProduct.city}</li>
          </ul>
          <Review reviewData={review} />
          <p>{transferType}</p>
        </article>
      ) : dataProduct.deletedAt && token ? (
        <article>
          <ProductImages idProduct={dataProduct.id} />
          <ul>
            <li>Nombre del producto: {dataProduct.productName}</li>
            <li>Precio: {dataProduct.price}</li>
            <li>Categoría: {dataProduct.category}</li>
            <li>Localidad: {dataProduct.city}</li>
          </ul>
          <p>{transferType}</p>
          <form>
            <button>
              <Link to={`/new-review/${dataProduct.id}/${user}`}>
                Haz una reseña
              </Link>
            </button>
          </form>
        </article>
      ) : token ? (
        <article>
          <ProductImages idProduct={dataProduct.id} />
          <ul>
            <li>Nombre del producto: {dataProduct.productName}</li>
            <li>Precio: {dataProduct.price}</li>
            <li>Categoría: {dataProduct.category}</li>
            <li>Localidad: {dataProduct.city}</li>
          </ul>
          <Link to={`/product/update/${dataProduct.id}`}>
            Ir a {dataProduct.productName}
          </Link>
        </article>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
};
