import { useContext } from "react";
import { TokenContext } from "../Components/TokenContext";
import { HomeLoged } from "../Components/HomeLoged";
import { HomePublic } from "../Components/HomePublic";

export function Home() {
  const [token] = useContext(TokenContext);

  return <div className="home">{token ? <HomeLoged /> : <HomePublic />}</div>;
}
