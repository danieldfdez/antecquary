import { ProductsHistorySell } from "./ProductsHistorySell";
import { ProductsHistoryBuy } from "./ProductsHistoryBuy";

export const HistoryUser = (props) => {
  return (
    <section>
      <ProductsHistorySell></ProductsHistorySell>
      <ProductsHistoryBuy></ProductsHistoryBuy>
    </section>
  );
};
