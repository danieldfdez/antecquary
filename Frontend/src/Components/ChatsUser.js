import { useContext, useEffect, useState } from "react";
import { TokenContext } from "./TokenContext";
import { useProfile } from "../hooks/remoteHooks";
import { Redirect, Link } from "react-router-dom";

export const ChatsUser = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const [data] = useProfile(token);
  const [chats, setChats] = useState([]);
  const [count, setCount] = useState(0);
  const indexMap = count * 8;
  useEffect(() => {
    const getChats = async () => {
      const res = await (
        await fetch("http://localhost:3000/api/v1/chats", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
      ).json();
      if (res.error) {
        setErrorMsg(res.error);
      } else {
        setChats(res);
        setErrorMsg(null);
      }
    };
    getChats();
  }, [token]);

  if (!chats) return <></>;
  if (!data) return <></>;

  const next = (e) => {
    e.preventDefault();
    if (indexMap >= chats.length - 8) {
      setCount(0);
    } else {
      setCount(count + 1);
    }
  };

  const previous = (e) => {
    e.preventDefault();
    if (indexMap === 0) {
      return true;
    } else {
      setCount(count - 1);
    }
  };
  const listItems = chats
    .filter((chat, index) => index >= indexMap && index < indexMap + 8)
    .map((chat, index) => {
      let idUser;
      if (data.id === chat.idSeller) {
        idUser = chat.idClient;
      } else if (data.id === chat.idClient) {
        idUser = chat.idSeller;
      }
      return (
        <li>
          <Link to={`/chat/${chat.id}/${chat.idProduct}/${idUser}`}>
            <p>{`${chat.productName} ${chat.city}`}</p>
          </Link>
          ;
        </li>
      );
    });

  return (
    <div className="center">
      <div className="section-background">
        <section>
          {errorMsg ? (
            <div style={{ color: "red" }}>{errorMsg}</div>
          ) : token ? (
            <>
              <ul>{listItems}</ul>
              <form>
                <button className="button-all-page" onClick={previous}>
                  Anterior
                </button>
                <button className="button-all-page" onClick={next}>
                  Siguiente
                </button>
              </form>
            </>
          ) : (
            <Redirect to="/login" />
          )}
        </section>
      </div>
    </div>
  );
};
