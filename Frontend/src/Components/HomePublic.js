import { useState, useEffect } from "react";
import { Carrousel } from "./Carrousel";

export function HomePublic() {
  const [errorMsgRecentProducts, setErrorMsgRecentProducts] = useState(null);
  const [errorMsgPopularProducts, setErrorMsgPopularProducts] = useState(null);
  const [errorMsgProductsPrice, setErrorMsgProductsPrice] = useState(null);
  const [recentProducts, setRecentProducts] = useState([]);
  const [popularProducts, setPopularProducts] = useState([]);
  const [productsByPrice, setProductsByPrice] = useState([]);

  useEffect(() => {
    async function loadProducts() {
      const recentProductsList = await (
        await fetch("http://localhost:3000/api/v1/products/sortBy/recent")
      ).json();
      if (recentProductsList.error) {
        setErrorMsgRecentProducts(`${recentProductsList.error}`);
      } else {
        setErrorMsgRecentProducts(null);
        setRecentProducts(recentProductsList);
      }

      const popularProductsList = await (
        await fetch("http://localhost:3000/api/v1/products/sortBy/popular")
      ).json();
      if (popularProductsList.error) {
        setErrorMsgPopularProducts(`${popularProductsList.error}`);
      } else {
        setErrorMsgPopularProducts(null);
        setPopularProducts(popularProductsList);
      }

      const productsByPriceList = await (
        await fetch("http://localhost:3000/api/v1/products/sortBy/5000")
      ).json();
      if (productsByPriceList.error) {
        setErrorMsgProductsPrice(`${productsByPriceList.error}`);
      } else {
        setErrorMsgProductsPrice(null);
        setProductsByPrice(productsByPriceList);
      }
    }
    loadProducts();
  }, []);
  if (!recentProducts) return <>....</>;
  if (!popularProducts) return <>....</>;
  if (!productsByPrice) return <>....</>;

  return (
    <div className="home">
      <div className="carrousel-container">
        <h1>Productos más recientes</h1>
        {errorMsgRecentProducts ? (
          <div style={{ color: "red" }}>{errorMsgRecentProducts}</div>
        ) : (
          <Carrousel productsData={recentProducts}></Carrousel>
        )}
      </div>
      <div className="carrousel-container">
        <h1>Productos más populares</h1>
        {errorMsgPopularProducts ? (
          <div style={{ color: "red" }}>{errorMsgPopularProducts}</div>
        ) : (
          <Carrousel productsData={popularProducts}></Carrousel>
        )}
      </div>
      <div className="carrousel-container">
        <h1>Productos más baratos</h1>
        {errorMsgProductsPrice ? (
          <div style={{ color: "red" }}>{errorMsgProductsPrice}</div>
        ) : (
          <Carrousel productsData={productsByPrice}></Carrousel>
        )}
      </div>
    </div>
  );
}
