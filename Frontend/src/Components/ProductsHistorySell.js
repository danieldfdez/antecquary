import { useContext, useEffect, useState } from "react";
import { TokenContext } from "./TokenContext";
import { LittleProduct } from "./LittleProduct";
import { useProductsHistorySell } from "../hooks/remoteHooks";
import { Redirect } from "react-router-dom";

export const ProductsHistorySell = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const [productsHistorySell] = useProductsHistorySell(token);
  const [products, setProducts] = useState([]);
  const [count, setCount] = useState(0);
  const indexMap = count * 4;
  useEffect(() => {
    if (productsHistorySell.error) {
      setErrorMsg(productsHistorySell.error);
    } else {
      setProducts(productsHistorySell);
      setErrorMsg(null);
    }
  }, [productsHistorySell]);

  const next = (e) => {
    e.preventDefault();
    if (indexMap >= products.length - 4) {
      setCount(0);
    } else {
      setCount(count + 1);
    }
  };

  const previous = (e) => {
    e.preventDefault();
    if (indexMap === 0) {
      return true;
    } else {
      setCount(count - 1);
    }
  };
  const listItems = products
    .filter((product, index) => index >= indexMap && index < indexMap + 4)
    .map((product, index) => {
      return (
        <LittleProduct
          key={`idLittleProductSell${index}`}
          dataProduct={product}
          transferType="venta"
        />
      );
    });

  return (
    <section>
      {errorMsg ? (
        <div style={{ color: "red" }}>{errorMsg}</div>
      ) : token ? (
        <>
          {listItems}
          <form>
            <button onClick={previous}>Anterior</button>
            <button onClick={next}>Siguiente</button>
          </form>
        </>
      ) : (
        <Redirect to="/login" />
      )}
    </section>
  );
};
