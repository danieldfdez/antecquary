import { useContext, useState, useEffect } from "react";
import { TokenContext } from "./TokenContext";
import { useProfile } from "../hooks/remoteHooks";
import { ProductImages } from "./ProductImages";
import { LittleUser } from "./LittleUser";
import { CreateChat } from "./CreateChat";

export function Product({ idProduct }) {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const [product, setProduct] = useState([]);
  const [data] = useProfile(token);

  useEffect(() => {
    async function loadProduct() {
      const res = await (
        await fetch(`http://localhost:3000/api/v1/products/id/${idProduct}`)
      ).json();
      if (res.error) {
        setErrorMsg(`${res.error}`);
      } else {
        setErrorMsg(null);
        setProduct(res);
      }
    }
    loadProduct();
  }, [idProduct]);
  if (!product) return <>....</>;
  if (!data) return <>....</>;

  return (
    <div className="center">
      <>
        <article className="section-background">
          <ProductImages id={`${idProduct}`}></ProductImages>
          <section className="product-content">
            <div>Nombre : {`${product.productName}`}</div>
            <div>Precio : {`${product.price}`}</div>
            <div>Localidad : {`${product.city}`}</div>
            <div>Categoría : {`${product.category}`}</div>
          </section>
          <LittleUser idUser={product.idSeller}></LittleUser>
          <section className="product-content">
            <div>Marca : {`${product.brand}`}</div>
            <div>Año de adquisición : {`${product.yearOfPurchase}`}</div>
            <div>Año de lanzamiento : {`${product.yearOfLaunch}`}</div>
            <div>Estado actual : {`${product.actualStatus}`}</div>
            <div>Documentación original : {`${product.originalDocuments}`}</div>
            <div>Embalaje original : {`${product.originalPackage}`}</div>
          </section>
          <section className="product-content">
            <div>Accesorios : {`${product.accesories}`}</div>
            <div>
              Descripción del producto : {`${product.descriptionProduct}`}
            </div>
          </section>
          <CreateChat idProduct={idProduct} idUser={data.id}></CreateChat>
          {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
        </article>
      </>
    </div>
  );
}
