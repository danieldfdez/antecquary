import { useContext, useEffect, useState } from "react";
import { TokenContext } from "./TokenContext";
import { LittleProduct } from "./LittleProduct";
import { useProductsHistoryBuy } from "../hooks/remoteHooks";
import { Redirect } from "react-router-dom";

export const ProductsHistoryBuy = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const [productsHistoryBuy] = useProductsHistoryBuy(token);
  const [products, setProducts] = useState([]);
  const [count, setCount] = useState(0);
  const indexMap = count * 4;
  useEffect(() => {
    if (productsHistoryBuy.error) {
      setErrorMsg(productsHistoryBuy.error);
    } else {
      setProducts(productsHistoryBuy);
      setErrorMsg(null);
    }
  }, [productsHistoryBuy]);

  const next = (e) => {
    e.preventDefault();
    if (indexMap >= products.length - 4) {
      setCount(0);
    } else {
      setCount(count + 1);
    }
  };

  const previous = (e) => {
    e.preventDefault();
    if (indexMap === 0) {
      return true;
    } else {
      setCount(count - 1);
    }
  };
  const listItems = products
    .filter((product, index) => index >= indexMap && index < indexMap + 4)
    .map((product, index) => {
      return (
        <LittleProduct
          key={`idLittleProductBuy${index}`}
          dataProduct={product}
          transferType="compra"
        />
      );
    });

  return (
    <section>
      {errorMsg ? (
        <div style={{ color: "red" }}>{errorMsg}</div>
      ) : token ? (
        <>
          {listItems}
          <form>
            <button onClick={previous}>Anterior</button>
            <button onClick={next}>Siguiente</button>
          </form>
        </>
      ) : (
        <Redirect to="/login" />
      )}
    </section>
  );
};
