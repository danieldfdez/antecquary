import React from "react";
import { Link } from "react-router-dom";

export function Button() {
  return (
    <Link to="login">
      <button className="signButton">INICIA SESION</button>
    </Link>
  );
}
