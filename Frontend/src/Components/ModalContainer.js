import React, { useState } from "react";
import { ProductImagesLittle } from "./ProductImagesPequeno";
import ModalProducts from "./ModalProducts";

export const ModalContainer = ({ productData }) => {
  const [showModal, setShowModal] = useState(false);
  const openModal = () => {
    setShowModal((prev) => !prev);
  };

  return (
    <div className="modal-container" onClick={openModal}>
      <ProductImagesLittle
        imgClass="modal-image"
        idProduct={productData.id}
      ></ProductImagesLittle>
      <ModalProducts
        productData={productData}
        showModal={showModal}
        setShowModal={setShowModal}
      />
    </div>
  );
};
