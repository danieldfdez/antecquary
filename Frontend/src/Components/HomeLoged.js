import { useState, useEffect } from "react";
import { useContext } from "react";
import { TokenContext } from "./TokenContext";
import { Carrousel } from "./Carrousel";
import { useProfile, useUserProducts } from "../hooks/remoteHooks";

export function HomeLoged() {
  const [errorMsgFavoriteProducts, setErrorMsgFavoriteProducts] =
    useState(null);
  const [errorMsgVisitedProducts, setErrorMsgVisitedProducts] = useState(null);
  const [errorMsgRecentProducts, setErrorMsgRecentProducts] = useState(null);
  const [errorMsgPopularProducts, setErrorMsgPopularProducts] = useState(null);
  const [errorMsgProductsPrice, setErrorMsgProductsPrice] = useState(null);
  const [token] = useContext(TokenContext);
  const [data] = useProfile(token);

  const [userProducts, errorMsgUserProducts] = useUserProducts(data.id);
  const [favoriteProducts, setFavoriteProducts] = useState([]);
  const [visitedProducts, setVisitedProducts] = useState([]);
  const [recentProducts, setRecentProducts] = useState([]);
  const [popularProducts, setPopularProducts] = useState([]);
  const [productsByPrice, setProductsByPrice] = useState([]);

  useEffect(() => {
    async function loadProducts() {
      const favoriteProductsList = await (
        await fetch("http://localhost:3000/api/v1/products/favorite", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
      ).json();
      if (favoriteProductsList.error) {
        setErrorMsgFavoriteProducts(`${favoriteProductsList.error}`);
      } else {
        setErrorMsgFavoriteProducts(null);
        setFavoriteProducts(favoriteProductsList);
      }

      const productsVisited = await (
        await fetch("http://localhost:3000/api/v1/products/visited", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
      ).json();
      if (productsVisited.error) {
        setErrorMsgVisitedProducts(`${productsVisited.error}`);
      } else {
        setErrorMsgVisitedProducts(null);
        setVisitedProducts(productsVisited);
      }

      const recentProductsList = await (
        await fetch("http://localhost:3000/api/v1/products/sortBy/recent")
      ).json();
      if (recentProductsList.error) {
        setErrorMsgRecentProducts(`${recentProductsList.error}`);
      } else {
        setErrorMsgRecentProducts(null);
        setRecentProducts(recentProductsList);
      }

      const popularProductsList = await (
        await fetch("http://localhost:3000/api/v1/products/sortBy/popular")
      ).json();
      if (popularProductsList.error) {
        setErrorMsgPopularProducts(`${popularProductsList.error}`);
      } else {
        setErrorMsgPopularProducts(null);
        setPopularProducts(popularProductsList);
      }

      const productsByPriceList = await (
        await fetch("http://localhost:3000/api/v1/products/sortBy/5000")
      ).json();
      if (productsByPriceList.error) {
        setErrorMsgProductsPrice(`${productsByPriceList.error}`);
      } else {
        setErrorMsgProductsPrice(null);
        setProductsByPrice(productsByPriceList);
      }
    }
    loadProducts();
  }, [token]);
  if (!data) return <></>;
  if (!userProducts) return <>....</>;
  if (!favoriteProducts) return <>....</>;
  if (!visitedProducts) return <>....</>;
  if (!recentProducts) return <>....</>;
  if (!popularProducts) return <>....</>;
  if (!productsByPrice) return <>....</>;

  return (
    <div className="home">
      <div className="carrousel-container">
        <h1>Tus productos a la venta</h1>
        {errorMsgUserProducts ? (
          <div style={{ color: "red" }}>{errorMsgUserProducts}</div>
        ) : (
          <Carrousel productsData={userProducts}></Carrousel>
        )}
      </div>
      <div className="carrousel-container">
        <h1>Tus productos favoritos</h1>
        {errorMsgFavoriteProducts ? (
          <div style={{ color: "red" }}>{errorMsgFavoriteProducts}</div>
        ) : (
          <Carrousel productsData={favoriteProducts}></Carrousel>
        )}
      </div>
      <div className="carrousel-container">
        <h1>Últimos artículos visitados</h1>
        {errorMsgVisitedProducts ? (
          <div style={{ color: "red" }}>{errorMsgVisitedProducts}</div>
        ) : (
          <Carrousel productsData={visitedProducts}></Carrousel>
        )}
      </div>
      <div className="carrousel-container">
        <h1>Productos más recientes</h1>
        {errorMsgRecentProducts ? (
          <div style={{ color: "red" }}>{errorMsgRecentProducts}</div>
        ) : (
          <Carrousel productsData={recentProducts}></Carrousel>
        )}
      </div>
      <div className="carrousel-container">
        <h1>Productos más populares</h1>
        {errorMsgPopularProducts ? (
          <div style={{ color: "red" }}>{errorMsgPopularProducts}</div>
        ) : (
          <Carrousel productsData={popularProducts}></Carrousel>
        )}
      </div>
      <div className="carrousel-container">
        <h1>Productos más baratos</h1>
        {errorMsgProductsPrice ? (
          <div style={{ color: "red" }}>{errorMsgProductsPrice}</div>
        ) : (
          <Carrousel productsData={productsByPrice}></Carrousel>
        )}
      </div>
    </div>
  );
}
