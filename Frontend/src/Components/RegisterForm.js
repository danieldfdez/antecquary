import { useState } from "react";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";

export const RegisterForm = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [birthday, setBirthday] = useState("");
  const [sex, setSex] = useState("no especificar");
  const [NIF, setNIF] = useState("");
  const [country, setCountry] = useState("");
  const [city, setCity] = useState("");
  const [adress, setAdress] = useState("");
  const [CP, setCP] = useState("");
  const [phone, setPhone] = useState("");
  const [nickname, setNickname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");
  const [link, setLink] = useState();
  const handleRegister = async (e) => {
    e.preventDefault();
    const requestBody = {
      firstName,
      lastName,
      birthday,
      sex,
      NIF,
      country,
      city,
      adress,
      CP,
      phone,
      nickname,
      email,
      password,
      repeatPassword,
    };

    const res = await fetch("http://localhost:3000/api/v1/users/register", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(requestBody),
    });
    const bodyDeLaRespuesta = await res.json();
    if (res.ok) {
      setErrorMsg(null);
      setLink(bodyDeLaRespuesta);
    } else {
      setErrorMsg(`${bodyDeLaRespuesta.error}`);
    }
  };
  return (
    <>
      {link ? (
        <Redirect to="/login" />
      ) : (
        <div className="center">
          <form onSubmit={handleRegister} className="user-form">
            <h1>Registro</h1>
            <fieldset>
              <h2>Datos Personales</h2>
              <label htmlFor="user-firstname">Nombre</label>
              <input
                id="user-firstname"
                name="user-firstname"
                required="required"
                type="text"
                placeholder="Nombre"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
              <label htmlFor="user-lastname">Apellidos</label>
              <input
                id="user-lastname"
                name="user-lastname"
                required="required"
                type="text"
                placeholder="Apellidos"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
              <label htmlFor="user-birthdate" type="birthday">
                Fecha de Nacimiento
              </label>
              <input
                id="user-birthdate"
                name="user-birthdate"
                type="date"
                placeholder="mm/dd/aaaa"
                value={birthday}
                onChange={(e) => setBirthday(e.target.value)}
              />
              <label htmlFor="user-nif">NIF</label>
              <input
                id="user-nif"
                name="user-nif"
                required="required"
                type="text"
                placeholder="NIF"
                value={NIF}
                onChange={(e) => setNIF(e.target.value)}
              />
              <label htmlFor="user-sex">Sexo</label>
              <select
                id="user-sex"
                name="user-sex"
                required="required"
                value={sex}
                onChange={(e) => setSex(e.target.value)}
              >
                <option>hombre</option>
                <option>mujer</option>
                <option>no especificar</option>
              </select>
              <label htmlFor="user-country">País</label>
              <input
                id="user-country"
                name="user-country"
                required="required"
                type="text"
                placeholder="Ej:España"
                value={country}
                onChange={(e) => setCountry(e.target.value)}
              />
              <label htmlFor="user-city">Ciudad</label>
              <input
                id="user-city"
                name="user-city"
                required="required"
                type="text"
                placeholder="Ciudad"
                value={city}
                onChange={(e) => setCity(e.target.value)}
              />
              <label htmlFor="user-adress">Dirección</label>
              <input
                id="user-adress"
                name="user-adress"
                required="required"
                type="text"
                placeholder="Avenida/Calle/Numero"
                value={adress}
                onChange={(e) => setAdress(e.target.value)}
              />
              <label htmlFor="user-cp">Código Postal</label>
              <input
                id="user-cp"
                name="user-cp"
                required="required"
                type="number"
                placeholder="Código Postal"
                value={CP}
                onChange={(e) => setCP(e.target.value)}
              />
              <label htmlFor="user-phone">Teléfono</label>
              <input
                id="user-phone"
                name="user-phone"
                type="text"
                placeholder="Teléfono"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
            </fieldset>
            <fieldset>
              <h2>Datos de tu Cuenta</h2>
              <label htmlFor="user-nickname">Nombre de la cuenta</label>
              <input
                id="user-nickname"
                name="user-nickname"
                required="required"
                type="text"
                placeholder="Nickname"
                value={nickname}
                onChange={(e) => setNickname(e.target.value)}
              />
              <label htmlFor="user-email">Correo electrónico</label>
              <input
                id="user-email"
                name="user-email"
                required="required"
                type="email"
                placeholder="ejemplo@mail.com"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <label htmlFor="user-password">Contraseña</label>
              <input
                id="user-password"
                name="user-password"
                required="required"
                type="password"
                placeholder="ej. X8df!90EO"
                pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                title="Debe contener al menos un número y una letra minúscula y mayúscula, y al menos 8 o más caracteres"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <label htmlFor="user-confirm-password">
                Confirma tu contraseña
              </label>
              <input
                id="user-confirm-password"
                name="user-confirm-password"
                required="required"
                type="password"
                placeholder="ej. X8df!90EO"
                value={repeatPassword}
                onChange={(e) => setRepeatPassword(e.target.value)}
              />
            </fieldset>
            <div className="acept-politics">
              <label htmlFor="politics">Acepto las Politicas</label>
              <input
                id="politics"
                name="politics"
                required="required"
                type="checkbox"
              />
              <label htmlFor="politica">
                <Link className="link-politics" to="/help">
                  Politicas, términos y condiciones
                </Link>
              </label>
            </div>
            <button type="submit" value="Registro" className="button-all-page">
              REGISTRO
            </button>
          </form>
          {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
        </div>
      )}
    </>
  );
};
