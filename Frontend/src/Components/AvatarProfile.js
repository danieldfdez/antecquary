import { useState, useContext, useEffect } from "react";
import { TokenContext } from "./TokenContext";
import { useProfile } from "../hooks/remoteHooks";

function AvatarProfile(props) {
  const [token] = useContext(TokenContext);
  const [data] = useProfile(token);
  const [file, setFile] = useState("");

  useEffect(() => {
    setFile(data.image);
  }, [data]);

  const imgSrc2 = `/images/profiles/${file}`;
  return (
    <div
      className="avatar"
      style={{ background: "pink", height: 80, width: 80 }}
    >
      <img style={{ width: 80 }} src={imgSrc2} alt="imagen de perfil" />
    </div>
  );
}

export { AvatarProfile };
