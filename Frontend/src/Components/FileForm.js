import { useContext, useState, useRef } from "react";
import { TokenContext } from "./TokenContext";

export const FileForm = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const imgRef = useRef();
  async function uploadFile() {
    const files = imgRef.current.files[0];
    let fileForm = new FormData();
    fileForm.append(props.fileName, files);
    const res = await fetch(props.url, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: fileForm,
    });
    const fileName = await res.json();
    if (res.ok) {
      setErrorMsg(null);
    } else {
      setErrorMsg(`${fileName.error}`);
    }
  }
  if (!token) return <></>;
  return (
    <>
      <div className="image-input" onSubmit={uploadFile}>
        <input
          className="input-button"
          type="file"
          ref={imgRef}
          accept="image/*,.jpeg,.jpg,.png"
        />
        <button className="button-all-page" type="submit">
          Enviar
        </button>
      </div>
      {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
    </>
  );
};
