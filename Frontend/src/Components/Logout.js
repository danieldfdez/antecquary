import { useContext } from "react";
import { TokenContext } from "./TokenContext";
import { Redirect } from "react-router-dom";

export const Logout = (props) => {
  const [token, setToken] = useContext(TokenContext);
  const handleLogout = (e) => {
    e.preventDefault();
    setToken("");
  };
  return (
    <>
      {token ? (
        <div>
          <form onSubmit={handleLogout}>
            <input
              type="submit"
              value="Logout"
              className="button-all-page"
            ></input>
          </form>
        </div>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
};
