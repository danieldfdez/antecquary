import { useContext, useState } from "react";
import { TokenContext } from "./TokenContext";
import { Link, Redirect } from "react-router-dom";

export const Login = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token, setToken] = useContext(TokenContext);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const handleLogin = async (e) => {
    e.preventDefault();
    const requestBody = {
      username,
      password,
    };
    const res = await fetch("http://localhost:3000/api/v1/users/login", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(requestBody),
    });
    const bodyDeLaRespuesta = await res.json();
    if (res.ok) {
      setToken(bodyDeLaRespuesta.accessToken);
      setErrorMsg(null);
    } else {
      setErrorMsg(`${bodyDeLaRespuesta.error}`);
    }
  };

  return (
    <>
      {token ? (
        <Redirect to="/profile" />
      ) : (
        <div className="login-container">
          <form onSubmit={handleLogin} className="user-form">
            <h1>Login</h1>
            <fieldset>
              <label htmlFor="username">Email o Nickname</label>
              <input
                name="username"
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
              <label htmlFor="password">Contraseña</label>
              <input
                name="password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </fieldset>
            <div>
              <input
                type="submit"
                value="Entrar"
                className="register-button"
              ></input>
            </div>
          </form>

          <Link to="/register">
            <input
              type="submit"
              value="Registrate"
              className="register-button-login"
            ></input>
          </Link>
          {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
        </div>
      )}
    </>
  );
};
