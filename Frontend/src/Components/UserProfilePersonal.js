import { useState, useContext, useEffect } from "react";
import { TokenContext } from "./TokenContext";
import { useProfile } from "../hooks/remoteHooks";
import { Redirect } from "react-router-dom";
import { Logout } from "./Logout";

export const UserProfilePersonal = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token, setToken] = useContext(TokenContext);
  const [data] = useProfile(token);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [birthday, setBirthday] = useState("");
  const [sex, setSex] = useState("no especificar");
  const [NIF, setNIF] = useState("");
  const [country, setCountry] = useState("");
  const [city, setCity] = useState("");
  const [adress, setAdress] = useState("");
  const [CP, setCP] = useState("");
  const [phone, setPhone] = useState("");
  const [nickname, setNickname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");

  useEffect(() => {
    setFirstName(data.firstName);
    setLastName(data.lastName);
    setBirthday(data.birthday);
    setSex(data.sex);
    setNIF(data.NIF);
    setCountry(data.country);
    setCity(data.city);
    setAdress(data.adress);
    setCP(data.CP);
    setPhone(data.phone);
    setNickname(data.nickname);
    setEmail(data.email);
  }, [data]);

  const handleUpdate = async (e) => {
    e.preventDefault();

    if (token) {
      const requestBody = [
        {
          firstName,
          lastName,
          birthday,
          sex,
          NIF,
          country,
          city,
          adress,
          CP,
          phone,
          nickname,
          email,
          password,
          repeatPassword,
        },
      ];

      const res = await fetch("http://localhost:3000/api/v1/users", {
        method: "PUT",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(requestBody),
      });
      if (res.ok) {
        setErrorMsg(null);
        setPassword("");
        setRepeatPassword("");
        if (email !== data.email) {
          setToken("");
        }
      } else {
        const bodyDeLaRespuesta = await res.json();
        setErrorMsg(`${bodyDeLaRespuesta.error}`);
      }
    }
  };

  if (!data) return <></>;
  return (
    <>
      {token ? (
        <div className="center">
          <form onSubmit={handleUpdate} className="user-form">
            <h1>Perfil</h1>
            <fieldset>
              Datos Personales
              <label htmlFor="user-firstname">Nombre</label>
              <input
                id="user-firstname"
                name="user-firstname"
                required="required"
                type="text"
                placeholder="Nombre"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
              <label htmlFor="user-lastname">Apellidos</label>
              <input
                id="user-lastname"
                name="user-lastname"
                required="required"
                type="text"
                placeholder="Apellidos"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
              <label htmlFor="user-birthdate" type="birthday">
                Fecha de Nacimiento
              </label>
              <input
                id="user-birthdate"
                name="user-birthdate"
                type="date"
                max="01/01/2100"
                placeholder="mm/dd/aaaa"
                value={birthday}
                onChange={(e) => setBirthday(e.target.value)}
              />
              <label htmlFor="user-nif">NIF</label>
              <input
                id="user-nif"
                name="user-nif"
                required="required"
                type="text"
                placeholder="NIF"
                value={NIF}
                onChange={(e) => setNIF(e.target.value)}
              />
              <label htmlFor="user-sex">Sexo</label>
              <select
                id="user-sex"
                name="user-sex"
                required="required"
                value={sex}
                onChange={(e) => setSex(e.target.value)}
              >
                <option value="hombre">hombre</option>
                <option value="mujer">mujer</option>
                <option value="no especificar">no especificar</option>
              </select>
              <label htmlFor="user-country">País</label>
              <input
                id="user-country"
                name="user-country"
                required="required"
                type="text"
                placeholder="Ej:España"
                value={country}
                onChange={(e) => setCountry(e.target.value)}
              />
              <label htmlFor="user-city">Ciudad</label>
              <input
                id="user-city"
                name="user-city"
                required="required"
                type="text"
                placeholder="Ciudad"
                value={city}
                onChange={(e) => setCity(e.target.value)}
              />
              <label htmlFor="user-adress">Dirección</label>
              <input
                id="user-adress"
                name="user-adress"
                required="required"
                type="text"
                placeholder="Avenida/Calle/Numero"
                value={adress}
                onChange={(e) => setAdress(e.target.value)}
              />
              <label htmlFor="user-cp">Código Postal</label>
              <input
                id="user-cp"
                name="user-cp"
                required="required"
                type="text"
                placeholder="Código Postal"
                value={CP}
                onChange={(e) => setCP(e.target.value)}
              />
              <label htmlFor="user-phone">Teléfono</label>
              <input
                id="user-phone"
                name="user-phone"
                type="text"
                placeholder="Teléfono"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
            </fieldset>
            <fieldset>
              Datos de tu Cuenta
              <label htmlFor="user-nickname">Nombre de la cuenta</label>
              <input
                id="user-nickname"
                name="user-nickname"
                required="required"
                type="text"
                placeholder="Nickname"
                value={nickname}
                onChange={(e) => setNickname(e.target.value)}
              />
              <label htmlFor="user-email">Correo electrónico</label>
              <input
                id="user-email"
                name="user-email"
                required="required"
                type="email"
                placeholder="ejemplo@mail.com"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <label htmlFor="user-password">Nueva contraseña</label>
              <input
                id="user-password"
                name="user-password"
                type="password"
                placeholder="ej. X8df!90EO"
                pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                title="Debe contener al menos un número y una letra minúscula y mayúscula, y al menos 8 o más caracteres"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <label htmlFor="user-confirm-password">
                Confirma tu nueva contraseña
              </label>
              <input
                id="user-confirm-password"
                name="user-confirm-password"
                type="password"
                placeholder="ej. X8df!90EO"
                value={repeatPassword}
                onChange={(e) => setRepeatPassword(e.target.value)}
              />
            </fieldset>

            <button
              type="submit"
              value="Actualizar Datos"
              className="button-all-page"
            >
              Actualizar Datos
            </button>
            <Logout />
          </form>
          {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
        </div>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
};
