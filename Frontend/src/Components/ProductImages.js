import { useState, useEffect } from "react";

export function ProductImages({ idProduct, imgClass }) {
  const [errorMsg, setErrorMsg] = useState(null);
  const [images, setImages] = useState([]);
  const [imgSrc, setImgSrc] = useState("");
  const [count, setCount] = useState(0);
  useEffect(() => {
    const getProductImages = async () => {
      const res = await (
        await fetch(`http://localhost:3000/api/v1/imagesProducts/${idProduct}`)
      ).json();
      if (res.error) {
        setErrorMsg(res.error);
      } else {
        setErrorMsg(null);
      }
      return res;
    };
    const findFile = async () => {
      const imageList = await getProductImages();
      setImages(imageList);
      if (imageList.error) {
        return true;
      } else {
        const findImage = imageList.find((image, index) => index === count);
        const src = `/images/products/${findImage.image}`;
        setImgSrc(src);
      }
    };
    findFile();
  }, [idProduct, count]);

  const next = (e) => {
    e.preventDefault();
    if (count === images.length - 1) {
      setCount(0);
    } else {
      setCount(count + 1);
    }
  };

  const previous = (e) => {
    e.preventDefault();
    if (count === 0) {
      setCount(images.length - 1);
    } else {
      setCount(count - 1);
    }
  };

  return (
    <>
      {errorMsg ? (
        <div style={{ color: "red" }}>{errorMsg}</div>
      ) : (
        <div className="product-image-container">
          <button
            style={{ width: 10, height: 80, textAlign: "center" }}
            onClick={previous}
          >
            ❮
          </button>
          <img className={imgClass} src={imgSrc} alt="imagen de producto" />
          <button
            style={{ width: 10, height: 80, textAlign: "center" }}
            onClick={next}
          >
            ❯
          </button>
        </div>
      )}
    </>
  );
}
