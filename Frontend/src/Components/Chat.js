import { useState, useEffect, useContext } from "react";
import { TokenContext } from "./TokenContext";
import { useLocalStorage } from "../hooks/useLocalStorage";
import { ChatRoomHeader } from "./ChatRoomHeader";
import { Message } from "./Message";
import { Redirect } from "react-router-dom";
import { useMessagesChat } from "../hooks/remoteHooks";
import { useUserProducts } from "../hooks/remoteHooks";

export const Chat = ({ idChat, idProduct, idUser }) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const [userProducts] = useUserProducts(token);
  const [product, setProduct] = useState();
  const [chatMessages] = useMessagesChat(token, idChat);
  const [content, setContent] = useLocalStorage("newMsg", "");
  const [listaMensajes, setListaMensajes] = useState([]);
  const [link, setLink] = useState();

  useEffect(() => {
    if (chatMessages.error) {
      setListaMensajes([]);
      setErrorMsg(chatMessages.error);
    } else {
      setListaMensajes(chatMessages);
      setErrorMsg(null);
    }
    if (userProducts.error) {
      setProduct("");
    } else {
      const productsUser = userProducts.find(
        (product) => `${product.id}` === idProduct
      );
      setProduct(productsUser);
      setErrorMsg(null);
    }
  }, [chatMessages, userProducts, idProduct]);

  const handleCreateMessage = async (e) => {
    e.preventDefault();
    const requestBody = {
      content,
    };
    const res = await fetch(
      `http://localhost:3000/api/v1/messages/idChat/${idChat}`,
      {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(requestBody),
      }
    );
    const bodyDeLaRespuesta = await res.json();
    if (res.ok) {
      setErrorMsg(null);
      const nuevaListaDeMensajes = [...listaMensajes, bodyDeLaRespuesta];
      setListaMensajes(nuevaListaDeMensajes);
      setContent("");
    } else {
      setErrorMsg(`${bodyDeLaRespuesta.error}`);
    }
  };

  const handleCreateTranfer = async (e) => {
    e.preventDefault();
    const resTransfer = await fetch(
      `http://localhost:3000/api/v1/transfers/idUser/${idUser}/idProduct/${idProduct}`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const transfer = await resTransfer.json();
    if (resTransfer.ok) {
      setErrorMsg(null);
    } else {
      setErrorMsg(`${transfer.error}`);
    }
    const resChat = await fetch(
      `http://localhost:3000/api/v1/chats/idProduct/${idProduct}`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const deleteChat = await resChat.json();
    if (resChat.ok) {
      setErrorMsg(null);
      setLink(deleteChat);
    } else {
      setErrorMsg(`${deleteChat.error}`);
    }
  };

  function actualizarInput(event) {
    const mensaje = event.target.value;
    setContent(mensaje);
  }

  const listItems = listaMensajes.map((mensaje, index) => {
    return <Message key={`${index}`} msg={mensaje}></Message>;
  });

  return (
    <>
      {link ? (
        <Redirect to="/profile" />
      ) : token && product ? (
        <div className="center">
          <div className="section-background">
            <ChatRoomHeader></ChatRoomHeader>
            {listItems}
            <form onSubmit={handleCreateMessage}>
              <input
                type="text"
                onChange={actualizarInput}
                value={content}
                name="escribe"
              ></input>
              <input
                type="submit"
                value="Enviar"
                className="button-all-page"
              ></input>
            </form>
            <form onSubmit={handleCreateTranfer}>
              <input
                type="submit"
                value="Confirmar venta"
                className="button-all-page"
              ></input>
            </form>
            {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
          </div>
        </div>
      ) : token ? (
        <div className="center">
          <div className="section-background">
            <ChatRoomHeader></ChatRoomHeader>
            {listItems}
            <form onSubmit={handleCreateMessage}>
              <input
                type="text"
                onChange={actualizarInput}
                value={content}
                name="escribe"
              ></input>
              <input
                type="submit"
                value="Enviar"
                className="button-all-page"
              ></input>
              {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
            </form>
          </div>
        </div>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
};
