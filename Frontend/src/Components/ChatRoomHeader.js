import { useState, useEffect } from "react";

function useDate() {
  const [count, setCount] = useState(new Date().toLocaleTimeString());
  useEffect(() => {
    const interval = setInterval(() => {
      setCount(new Date().toLocaleTimeString());
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [setCount]);
  return count;
}

const ChatRoomHeader = (props) => {
  const date = useDate();
  return <div>{date}</div>;
};

export { ChatRoomHeader };
