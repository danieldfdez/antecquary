const MessageBody = ({ userName, content }) => {
  return (
    <div style={{ background: "lightblue", width: "100%" }}>
      <div>{userName}</div>
      <div>{content}</div>
    </div>
  );
};

export { MessageBody };
