import { useState, useContext } from "react";
import { TokenContext } from "./TokenContext";
import { Redirect } from "react-router-dom";

export const CreateReview = ({ idProduct, idUser }) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const [content, setContent] = useState("");
  const [rating, setRating] = useState("");
  const [link, setLink] = useState();

  const handleCreateReview = async (e) => {
    e.preventDefault();
    const requestBody = {
      idUser,
      content,
      rating,
    };
    const res = await fetch(
      `http://localhost:3000/api/v1/reviews/idProduct/${idProduct}`,
      {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(requestBody),
      }
    );
    const bodyDeLaRespuesta = await res.json();
    if (res.ok) {
      setErrorMsg(null);
      setLink(bodyDeLaRespuesta);
    } else {
      setErrorMsg(`${bodyDeLaRespuesta.error}`);
    }
  };
  if (!idUser) return <></>;
  return (
    <>
      {link ? (
        <Redirect to="/historial" />
      ) : token ? (
        <form onSubmit={handleCreateReview}>
          <div>
            <label htmlFor="rating">Valoración</label>
            <select
              id="rating"
              name="rating"
              value={rating}
              onChange={(e) => setRating(e.target.value)}
            >
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
            </select>
          </div>
          <div>
            <label htmlFor="content-review">Comentario</label>
            <textarea
              id="content-review"
              name="content-review"
              className="content-review"
              cols="10"
              rows="10"
              placeholder="Descripción del producto"
              value={content}
              onChange={(e) => setContent(e.target.value)}
            ></textarea>
          </div>
          <button type="submit">Postear reseña</button>
          {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
        </form>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
};
