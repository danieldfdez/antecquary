import { useState, useEffect } from "react";
import { Carrousel } from "./Carrousel";
import GalleryProducts from "../Components/GalleryProducts";

export function ProductsSection({ category }) {
  const [errorMsgRecentProducts, setErrorMsgRecentProducts] = useState(null);
  const [errorMsgPopularProducts, setErrorMsgPopularProducts] = useState(null);
  const [errorMsgProductsPrice, setErrorMsgProductsPrice] = useState(null);
  const [recentProducts, setRecentProducts] = useState([]);
  const [popularProducts, setPopularProducts] = useState([]);
  const [productsByPrice, setProductsByPrice] = useState([]);

  useEffect(() => {
    async function loadProducts() {
      const recentProductsList = await (
        await fetch(
          `http://localhost:3000/api/v1/products/category/${category}/sortBy/recent`
        )
      ).json();
      if (recentProductsList.error) {
        setErrorMsgRecentProducts(`${recentProductsList.error}`);
      } else {
        setErrorMsgRecentProducts(null);
        setRecentProducts(recentProductsList);
      }

      const popularProductsList = await (
        await fetch(
          `http://localhost:3000/api/v1/products/category/${category}/sortBy/popular`
        )
      ).json();
      if (popularProductsList.error) {
        setErrorMsgPopularProducts(`${popularProductsList.error}`);
      } else {
        setErrorMsgPopularProducts(null);
        setPopularProducts(popularProductsList);
      }

      const productsByPriceList = await (
        await fetch(
          `http://localhost:3000/api/v1/products/category/${category}/sortBy/5000`
        )
      ).json();
      if (productsByPriceList.error) {
        setErrorMsgProductsPrice(`${productsByPriceList.error}`);
      } else {
        setErrorMsgProductsPrice(null);
        setProductsByPrice(productsByPriceList);
      }
    }
    loadProducts();
  }, [category]);
  if (!recentProducts) return <>....</>;
  if (!popularProducts) return <>....</>;
  if (!productsByPrice) return <>....</>;

  return (
    <div className={`${category}`}>
      <div className="gallery-container">
        <h1>Productos más recientes</h1>
        {errorMsgRecentProducts ? (
          <div style={{ color: "red" }}>{errorMsgRecentProducts}</div>
        ) : (
          <GalleryProducts productsData={recentProducts}></GalleryProducts>
        )}
      </div>
      <div className="gallery-container">
        <h1>Productos más populares</h1>
        {errorMsgPopularProducts ? (
          <div style={{ color: "red" }}>{errorMsgPopularProducts}</div>
        ) : (
          <GalleryProducts productsData={popularProducts}></GalleryProducts>
        )}
      </div>
      <div className="gallery-container">
        <h1>Productos más baratos</h1>
        {errorMsgProductsPrice ? (
          <div style={{ color: "red" }}>{errorMsgProductsPrice}</div>
        ) : (
          <GalleryProducts productsData={productsByPrice}></GalleryProducts>
        )}
      </div>
    </div>
  );
}
