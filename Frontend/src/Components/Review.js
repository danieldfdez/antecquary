import { Avatar } from "./Avatar";
import { useUserPublic } from "../hooks/remoteHooks";

const Review = ({ reviewData, idUser }) => {
  const [userData] = useUserPublic(idUser);
  if (!userData) return <></>;
  return (
    <>
      {idUser ? (
        <article style={{ display: "flex", padding: "10px" }}>
          <Avatar img={userData.image} />
          <p>{userData.nickname}</p>
          <p>Valoración : {reviewData.rating}</p>
          <p>Reseña : {reviewData.content}</p>
        </article>
      ) : (
        <article style={{ display: "flex", padding: "10px" }}>
          <p>Valoración : {reviewData.rating}</p>
          <p>Reseña : {reviewData.content}</p>
        </article>
      )}
    </>
  );
};

export { Review };
