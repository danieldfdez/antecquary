import { useState, useContext, useEffect } from "react";
import { TokenContext } from "./TokenContext";
import { Redirect } from "react-router-dom";
import { ProductImages } from "./ProductImages";
import { FileForm } from "./FileForm";

export const ProductUpdate = ({ idProduct }) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [token] = useContext(TokenContext);
  const [productName, setProductName] = useState("");
  const [price, setPrice] = useState("");
  const [city, setCity] = useState("");
  const [brand, setBrand] = useState("");
  const [yearOfLaunch, setYearOfLaunch] = useState("");
  const [yearOfPurchase, setYearOfPurchase] = useState("");
  const [category, setCategory] = useState("otros");
  const [actualStatus, setActualStatus] = useState("funcional");
  const [originalDocuments, setOriginalDocuments] = useState("no tiene");
  const [originalPackage, setOriginalPackage] = useState("no tiene");
  const [accesories, setAccesories] = useState("");
  const [descriptionProduct, setDescriptionProduct] = useState("");
  const urlFileForm = `http://localhost:3000/api/v1/imagesProducts/${idProduct}`;
  const fileName = "productImage";

  useEffect(() => {
    const getProduct = async () => {
      const res = await (
        await fetch(`http://localhost:3000/api/v1/products/id/${idProduct}`)
      ).json();
      if (res.error) {
        setErrorMsg(res.error);
      } else {
        setErrorMsg(null);
      }
      return res;
    };
    const paintProduct = async () => {
      const product = await getProduct();
      setProductName(product.productName);
      setPrice(product.price);
      setCity(product.city);
      setBrand(product.brand);
      setYearOfLaunch(product.yearOfLaunch);
      setYearOfPurchase(product.yearOfPurchase);
      setCategory(product.category);
      setActualStatus(product.actualStatus);
      setOriginalDocuments(product.originalDocuments);
      setOriginalPackage(product.originalPackage);
      setAccesories(product.accesories);
      setDescriptionProduct(product.descriptionProduct);
    };
    paintProduct();
  }, [idProduct]);

  const handleUpdateProduct = async (e) => {
    e.preventDefault();
    const requestBody = [
      {
        productName,
        price,
        city,
        brand,
        yearOfLaunch,
        yearOfPurchase,
        category,
        actualStatus,
        originalDocuments,
        originalPackage,
        accesories,
        descriptionProduct,
      },
    ];

    const res = await fetch(
      `http://localhost:3000/api/v1/products/id/${idProduct}`,
      {
        method: "PUT",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(requestBody),
      }
    );
    const bodyDeLaRespuesta = await res.json();
    if (res.ok) {
      setErrorMsg(null);
      setProductName(bodyDeLaRespuesta.productName);
      setPrice(bodyDeLaRespuesta.price);
      setCity(bodyDeLaRespuesta.city);
      setBrand(bodyDeLaRespuesta.brand);
      setYearOfLaunch(bodyDeLaRespuesta.yearOfLaunch);
      setYearOfPurchase(bodyDeLaRespuesta.yearOfPurchase);
      setCategory(bodyDeLaRespuesta.category);
      setActualStatus(bodyDeLaRespuesta.actualStatus);
      setOriginalDocuments(bodyDeLaRespuesta.originalDocuments);
      setOriginalPackage(bodyDeLaRespuesta.originalPackage);
      setAccesories(bodyDeLaRespuesta.accesories);
      setDescriptionProduct(bodyDeLaRespuesta.descriptionProduct);
    } else {
      setErrorMsg(`${bodyDeLaRespuesta.error}`);
    }
  };
  return (
    <>
      {token ? (
        <div className="update-product">
          <ProductImages idProduct={idProduct} />
          <FileForm url={urlFileForm} fileName={fileName} />
          <form onSubmit={handleUpdateProduct} className="product-form">
            <fieldset>
              Nuevo Producto
              <label htmlFor="product-name">Nombre del Producto</label>
              <input
                id="product-name"
                name="product-name"
                required="required"
                type="text"
                placeholder="Nombre del producto"
                value={productName}
                onChange={(e) => setProductName(e.target.value)}
              />
              <label htmlFor="price">Precio</label>
              <input
                id="price"
                name="price"
                type="text"
                required="required"
                placeholder="Precio del producto"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
              <label htmlFor="product-city">Ciudad</label>
              <input
                id="product-city"
                name="product-city"
                required="required"
                type="text"
                placeholder="Ciudad"
                value={city}
                onChange={(e) => setCity(e.target.value)}
              />
              <label htmlFor="brand">Marca</label>
              <input
                id="brand"
                name="brand"
                required="required"
                type="text"
                placeholder="Marca del producto"
                value={brand}
                onChange={(e) => setBrand(e.target.value)}
              />
              <label htmlFor="year-of-launch">Año de Lanzamiento</label>
              <input
                id="year-of-launch"
                name="year-of-launch"
                required="required"
                type="text"
                placeholder="Año de lanzamiento del producto"
                value={yearOfLaunch}
                onChange={(e) => setYearOfLaunch(e.target.value)}
              />
              <label htmlFor="year-of-purchase">Año de Adquisición</label>
              <input
                id="year-of-purchase"
                name="year-of-purchase"
                required="required"
                type="text"
                placeholder="Año de adquisición del producto"
                value={yearOfPurchase}
                onChange={(e) => setYearOfPurchase(e.target.value)}
              />
              <label htmlFor="category">Categoría</label>
              <select
                id="category"
                name="category"
                required="required"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              >
                <option>audiovisual</option>
                <option>videojuegos</option>
                <option>telefonía</option>
                <option>ordenadores</option>
                <option>cámaras</option>
                <option>otros</option>
              </select>
              <label htmlFor="actual-status" class="tipepro">
                Estado Actual
              </label>
              <select
                id="actual-status"
                name="actual-status"
                required="required"
                value={actualStatus}
                onChange={(e) => setActualStatus(e.target.value)}
              >
                <option>malo</option>
                <option>defectuoso</option>
                <option>muy usado</option>
                <option>desgastado</option>
                <option>funcional</option>
                <option>bueno</option>
                <option>perfecto</option>
                <option>sin uso</option>
              </select>
              <label htmlFor="original-documents">Documentación Original</label>
              <select
                id="original-documents"
                name="original-documents"
                required="required"
                value={originalDocuments}
                onChange={(e) => setOriginalDocuments(e.target.value)}
              >
                <option>no tiene</option>
                <option>en mal estado</option>
                <option>en buen estado</option>
                <option>faltan partes</option>
                <option>completa</option>
                <option>impecable</option>
              </select>
              <label htmlFor="original-package">Embalaje Original</label>
              <select
                id="original-package"
                name="original-package"
                required="required"
                value={originalPackage}
                onChange={(e) => setOriginalPackage(e.target.value)}
              >
                <option>no tiene</option>
                <option>en mal estado</option>
                <option>en buen estado</option>
                <option>impecable</option>
              </select>
              <label htmlFor="accesories">Accesorios</label>
              <textarea
                id="accesories"
                name="accesories"
                className="accesories"
                cols="2"
                rows="2"
                placeholder="Accesorios del producto"
                value={accesories}
                onChange={(e) => setAccesories(e.target.value)}
              ></textarea>
              <label htmlFor="description-product">Descripción</label>
              <textarea
                id="description-product"
                name="description-product"
                className="description-product"
                cols="10"
                rows="10"
                placeholder="Descripción del producto"
                value={descriptionProduct}
                onChange={(e) => setDescriptionProduct(e.target.value)}
              ></textarea>
            </fieldset>

            <button className="register-button" type="submit" value="Modificar">
              Modificar producto
            </button>
          </form>
          {errorMsg && <div style={{ color: "red" }}>{errorMsg}</div>}
        </div>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
};
