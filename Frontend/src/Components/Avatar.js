function Avatar({ img }) {
  const imgSrc2 = `/images/profiles/${img}`;
  return (
    <div className="avatar">
      <img src={imgSrc2} alt="imagen de perfil" />
    </div>
  );
}

export { Avatar };
