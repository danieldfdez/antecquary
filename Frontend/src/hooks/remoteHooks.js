import { useEffect, useState } from "react";

const useProfile = (token) => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const getProfile = async () => {
      const res = await (
        await fetch("http://localhost:3000/api/v1/users/profile", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
      ).json();
      setData(res);
    };
    getProfile();
  }, [token]);
  return [data];
};

const useUserPublic = (idUser) => {
  const [userData, setUserData] = useState([]);
  useEffect(() => {
    const getUserData = async () => {
      const res = await (
        await fetch(`http://localhost:3000/api/v1/users/idUser/${idUser}`)
      ).json();
      setUserData(res);
    };
    getUserData();
  }, [idUser]);
  return [userData];
};

const useUserProducts = (idSeller) => {
  const [userProducts, setUserProducts] = useState([]);
  const [errorMsgUserProducts, setErrorMsgUserProducts] = useState(null);
  useEffect(() => {
    const getProducts = async () => {
      const res = await (
        await fetch(
          `http://localhost:3000/api/v1/products/idSeller/${idSeller}`
        )
      ).json();
      if (res.error) {
        setErrorMsgUserProducts(`${res.error}`);
      } else {
        setErrorMsgUserProducts(null);
        setUserProducts(res);
      }
    };
    getProducts();
  }, [idSeller]);
  return [userProducts, errorMsgUserProducts];
};

const useProductsHistorySell = (token) => {
  const [productsHistorySell, setProductsHistorySell] = useState([]);
  useEffect(() => {
    const getProducts = async () => {
      const res = await (
        await fetch("http://localhost:3000/api/v1/products/selled", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
      ).json();
      setProductsHistorySell(res);
    };
    getProducts();
  }, [token]);
  return [productsHistorySell];
};

const useProductsHistoryBuy = (token) => {
  const [productsHistoryBuy, setProductsHistoryBuy] = useState([]);
  useEffect(() => {
    const getProducts = async () => {
      const res = await (
        await fetch("http://localhost:3000/api/v1/products/bought", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
      ).json();
      setProductsHistoryBuy(res);
    };
    getProducts();
  }, [token]);
  return [productsHistoryBuy];
};

const useTransferByProduct = (idProduct) => {
  const [transfer, setTransfer] = useState([]);
  useEffect(() => {
    const getTransfer = async () => {
      const res = await (
        await fetch(
          `http://localhost:3000/api/v1/transfers/idProduct/${idProduct}`
        )
      ).json();
      setTransfer(res);
    };
    getTransfer();
  }, [idProduct]);
  return [transfer];
};

const useReviewByIdProductAndIdReviewer = (idProduct, token) => {
  const [review, setReview] = useState([]);
  useEffect(() => {
    const getReview = async () => {
      const res = await (
        await fetch(
          `http://localhost:3000/api/v1/reviews/idProduct/${idProduct}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        )
      ).json();
      setReview(res);
    };
    getReview();
  }, [idProduct, token]);
  return [review];
};

const useMessagesChat = (token, idChat) => {
  const [chatMessages, setChatMessages] = useState([]);
  useEffect(() => {
    async function getMessages() {
      const messages = await (
        await fetch(`http://localhost:3000/api/v1/messages/idChat/${idChat}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
      ).json();
      setChatMessages(messages);
    }
    const interval = setInterval(() => {
      getMessages();
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [idChat, token]);
  return [chatMessages];
};

export {
  useProfile,
  useUserProducts,
  useMessagesChat,
  useUserPublic,
  useProductsHistorySell,
  useProductsHistoryBuy,
  useTransferByProduct,
  useReviewByIdProductAndIdReviewer,
};
